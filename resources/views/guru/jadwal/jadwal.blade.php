@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jadwal Mengajar
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-danger">
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td>Hari</td>
                                  <td>Waktu</td>
                                  <td>Kelas</td>
                                  <td>Mata Pelajaran</td>
                                  <td>Jumlah Jam</td>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($hasil as $item)
                                  <tr>
                                      <td>
                                          @if ($item->hari->hari == "Senin")
                                              <span class="label label-primary">S</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Selasa")
                                              <span class="label label-success">S</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Rabu")
                                              <span class="label label-warning">R</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Kamis")
                                              <span class="label label-info">K</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Jumat")
                                              <span class="label label-danger">J</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Sabtu")
                                              <span class="label label-primary">S</span> {{ $item->hari->hari }}
                                          @endif

                                      </td>
                                      <td>{{ $item->jam_mulai }} - {{ $item->jam_selesai }}</td>
                                      <td>{{ $item->kode_kelas }}</td>
                                      <td>{{ $item->matpel->mata_pelajaran }}</td>
                                      <td>{{ $item->jam_ajar }} Jam</td>
                                  </tr>
                                @endforeach
                            </tbody>
                            <tbody>
                              <tr>
                                  <td align="center" colspan="4">Total Jam Mengajar</td>
                                  <td>{{ $jam }} Jam</td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
