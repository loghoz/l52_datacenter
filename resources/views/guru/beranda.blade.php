@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Beranda
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                {{--  PROFIL  --}}
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Profil</h3>
                        </div>
                        @foreach ($guru as $profil)
                        <div class="box-body">
                            <table>
                                <tr>
                                    <td width="100px">Nama</td>
                                    <td>: {{ $profil->nama }}</td>
                                </tr>
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td>: {{ $profil->guru_meta->jk }}</td>
                                </tr>
                                <tr>
                                    <td>TTL</td>
                                    <td>: {{ $profil->guru_meta->tempat_lahir }}, {{ $profil->guru_meta->tanggal_lahir }}</td>
                                </tr>
                                <tr>
                                    <td>NIG</td>
                                    <td>: {{ $profil->no_induk }}</td>
                                </tr>
                                <tr>
                                    <td>NUPTK</td>
                                    <td>: {{ $profil->guru_meta->nuptk }}</td>
                                </tr>
                                <tr>
                                    <td>Agama</td>
                                    <td>: {{ $profil->agama->agama }}</td>
                                </tr>
                                <tr>
                                    <td>Jabatan</td>
                                    <td>: {{ $profil->jabatan->jabatan }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>: {{ $profil->status->status }}</td>
                                </tr>
                                <tr>
                                    <td>Tahun Tugas</td>
                                    <td>: {{ $profil->guru_meta->tahun_tugas }}</td>
                                </tr>
                                <tr>
                                    <td>Ijazah</td>
                                    <td>: {{ $profil->ijazah->ijazah }} {{ $profil->guru_meta->ijazah_jurusan }}</td>
                                </tr>
                                <tr>
                                    <td valign="top">Alamat</td>
                                    <td>
                                        : {{ $profil->guru_meta->alamat }}<br>
                                        RT/RW : {{ $profil->guru_meta->rt }}/{{ $profil->guru_meta->rw }}<br>
                                        Desa : {{ $profil->guru_meta->desa }}<br>
                                        Kecamatan : {{ $profil->guru_meta->kecamatan }}<br>
                                        Kabupaten : {{ $profil->guru_meta->kabupaten }}<br>
                                        Provinsi : {{ $profil->guru_meta->provinsi }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kode POS</td>
                                    <td>: {{ $profil->guru_meta->kode_pos }}</td>
                                </tr>
                                <tr>
                                    <td>Telp / HP</td>
                                    <td>: {{ $profil->guru_meta->telp }}</td>
                                </tr>
                            </table>
                        </div>

                        <div class="box-footer">
                            <div class="pull-right">
                                <a href="{{ route('guru.profil.edit', $profil->id) }}" class="btn btn-primary">Ubah</a>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Wali Kelas</h3>
                        </div>
                        <div class="box-body">
                            <h3><center>
                            @forelse ($walkel as $profil)
                                {{ $profil->kelas->kode_kelas }}
                            @empty
                                -
                            @endforelse
                            </center></h3>
                        </div>
                    </div>

                    <div class="box box-danger">
                        <div class="box-header with-border">
                            <h3 class="box-title">Jadwal Hari Ini</h3>
                        </div>
                        <div class="box-body">
                            @forelse ($jadwal as $item)
                            {{ $item->jam_mulai }} - {{ $item->jam_selesai }}<br>
                            {{ $item->kode_kelas }} - {{ $item->matpel->mata_pelajaran }} - {{ $item->jam_ajar }} Jam<br>
                            <hr>
                            @empty
                                Tidak ada jam mengajar
                            @endforelse
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Profil</h3>
                        </div>
                        <div class="box-body">
                        </div>
                        <div class="box-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
