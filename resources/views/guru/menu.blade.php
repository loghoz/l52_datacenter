<div class="user-panel">
  <div class="pull-left image">
    <img src="{{ asset('img/guru') }}/{{ Cookie::get('photo') }}" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p>{{ Cookie::get('nama') }}</p>
	<a href="{{ url('/tahun') }}">T.A. {{ Cookie::get('tahun_ajaran') }}</a> 
  </div>
</div>

<ul class="sidebar-menu" data-widget="tree">
	<li class="header">MENU</li>
	
	{{--  BERANDA  --}}
	<li>
		<a href="{{ url('/guru/beranda') }}">
			<i class="fa fa-home"></i>
			<span>Beranda</span>
		</a>
	</li>

	{{--  DATA SISWA  --}}
	<li>
		<a href="{{ url('/guru/siswa') }}">
			<i class="fa fa-user"></i>
			<span>Data Siswa</span>
		</a>
	</li>

	{{--  JADWAL  --}}
	<li>
		<a href="{{ url('/guru/jadwal') }}">
			<i class="fa fa-calendar"></i>
			<span>Jadwal</span>
		</a>
	</li>

	{{--  Nilai  --}}
	<li>
		<a href="{{ url('/guru/nilai') }}">
			<i class="fa fa-book"></i>
			<span>Nilai</span>
		</a>
	</li>

	{{--  Absensi  --}}
	<li>
		<a href="{{ url('/guru/absensi') }}">
			<i class="fa fa-calendar"></i>
			<span>Absensi</span>
		</a>
	</li>

	@if (Cookie::get('walikelas')=='-')
	@else
		{{--  DATA WALI  --}}
		<li class="treeview">
			<a href="#">
				<i class="fa fa-file"></i>
				<span>Wali Kelas</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>

			<ul class="treeview-menu">
				<li><a href="{{ url('/guru/walikelas/siswa') }}"><i class="fa fa-user"></i>Siswa</a></li>
				<li><a href="{{ url('/guru/walikelas/absen') }}"><i class="fa fa-calendar"></i><span>Absensi</span></a></li>
				<li><a href="{{ url('/guru/walikelas/nilai') }}"><i class="fa fa-book"></i><span>Nilai</span></a></li>
				<li><a href="{{ url('/guru/walikelas/rapot') }}"><i class="fa fa-book"></i><span>Rapot</span></a></li>
				<li><a href="{{ url('/guru/walikelas/naik') }}"><i class="fa fa-arrow-up"></i><span>Kenaikan Kelas</span></a></li>
			</ul>
		</li>
	@endif
	

	{{--  pengaturan  --}}
	{{--  <li>
		<a href="{{ url('/guru/pengaturan') }}">
			<i class="fa fa-cog"></i>
			<span>Pengaturan</span>
		</a>
	</li>  --}}

</ul>