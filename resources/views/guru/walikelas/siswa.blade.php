@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Wali Kelas
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="box-body">
                      <div class="col-md-4">
                        {!! Form::open(['url' => 'guru/walikelas/siswa', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Nama Siswa ....']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                          {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                      </div>
                      <div class="col-md-4">
                            {!! Form::open(['url' => 'guru/walikelas/siswa', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Data Siswa', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                          </div>
                      <div class="col-md-4" align="right">
                        {{--  KOSONG  --}}
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($siswa as $item)
                <div class="col-md-4">
                    @include('guru.walikelas._profil', ['siswa' => $item])
                </div>
              @empty
              <div class="col-md-12 text-center">
                @if (isset($q))
                  <h1>:(</h1>
                  <p>Siswa tidak ditemukan.</p>
                @endif
                  <p><a href="{{ url('/guru/siswa') }}">Lihat semua data siswa <i class="fa fa-arrow-right"></i></a></p>
              </div>
            @endforelse
        </div>
        {{ $siswa->appends(compact('q', 'page'))->links() }}
    </section>
@endsection
