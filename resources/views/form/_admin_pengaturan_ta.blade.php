<div class="col-sm-12">

    <div class="form-group{{ $errors->has('tahun_ajaran') ? ' has-error' : '' }}">
        {!! Form::label('tahun_ajaran', 'Tahun Ajaran', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('tahun_ajaran', null, ['class' => 'form-control','placeholder'=>'Tahun Ajaran']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('tahun_ajaran') }}</small>
        </div>
    </div>

    <input type="hidden" name="tipe" value="ta">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
