{{--  DATA SISWA  --}}
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Form Data Siswa</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">

            {{--  NAMA  --}}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {!! Form::label('nama', '* Nama Lengkap', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                </div>
            </div>

            {{--  JENIS KELAMIN  --}}
            <div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }}">
                {!! Form::label('jk', '* Jenis Kelamin', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-4">
                    <label class="radio-inline">
                        {!! Form::radio('jk', 'laki-laki') !!} Laki - Laki
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="radio-inline">
                        {!! Form::radio('jk', 'perempuan') !!} Perempuan
                    </label>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                </div>
            </div>

            {{--  TEMPAT LAHIR  --}}
            <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tempat_lahir', '* Tempat Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('tempat_lahir', null, ['class' => 'form-control','placeholder'=>'Tempat Lahir']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
                </div>
            </div>

            {{--  TANGGAL LAHIR  --}}
            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tanggal_lahir', '* Tanggal Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::date('tanggal_lahir', null, ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                </div>
            </div>

            {{--  AGAMA  --}}
            <div class="form-group{{ $errors->has('agama_id') ? ' has-error' : '' }}">
                {!! Form::label('agama_id', '* Agama', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('agama_id',$agama ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('agama_id') }}</small>
                </div>
            </div>

            {{--  NIS  --}}
            <div class="form-group{{ $errors->has('no_induk') ? ' has-error' : '' }}">
                {!! Form::label('no_induk', '* NIS', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('no_induk', null, ['class' => 'form-control','placeholder'=>'No Induk Siswa']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('no_induk') }}</small>
                </div>
            </div>

            {{--  NISN  --}}
            <div class="form-group{{ $errors->has('nisn') ? ' has-error' : '' }}">
                {!! Form::label('nisn', 'NISN', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('nisn', null, ['class' => 'form-control','placeholder'=>'No Induk Siswa Nasional']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('nisn') }}</small>
                </div>
            </div>

            {{--  KELAS  --}}
            <div class="form-group{{ $errors->has('kelas_id') ? ' has-error' : '' }}">
                {!! Form::label('kelas_id', '* Kelas', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('kelas_id',$kelas ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kelas_id') }}</small>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            {{--  ALAMAT  --}}
            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                {!! Form::label('alamat', '* Alamat', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('alamat', null, ['class' => 'form-control','placeholder'=>'Alamat']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('alamat') }}</small>
                </div>
            </div>

            {{--  RT  --}}
            <div class="form-group{{ $errors->has('rt') ? ' has-error' : '' }}">
                {!! Form::label('rt', '* RT', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('rt', null, ['class' => 'form-control','placeholder'=>'RT']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('rt') }}</small>
                </div>
            </div>

            {{--  RW  --}}
            <div class="form-group{{ $errors->has('rw') ? ' has-error' : '' }}">
                {!! Form::label('rw', '* RW', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('rw', null, ['class' => 'form-control','placeholder'=>'RW']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('rw') }}</small>
                </div>
            </div>

            {{--  DESA  --}}
            <div class="form-group{{ $errors->has('desa') ? ' has-error' : '' }}">
                {!! Form::label('desa', '* Dusun / Desa', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('desa', null, ['class' => 'form-control','placeholder'=>'Dusun / Desa']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('desa') }}</small>
                </div>
            </div>

            {{--  KECAMATAN  --}}
            <div class="form-group{{ $errors->has('kecamatan') ? ' has-error' : '' }}">
                {!! Form::label('kecamatan', '* Kecamatan', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('kecamatan', null, ['class' => 'form-control','placeholder'=>'Kecamatan']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kecamatan') }}</small>
                </div>
            </div>

            {{--  KABUPATEN  --}}
            <div class="form-group{{ $errors->has('kabupaten') ? ' has-error' : '' }}">
                {!! Form::label('kabupaten', '* Kabupaten / Kota', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('kabupaten', null, ['class' => 'form-control','placeholder'=>'Kabupaten / Kota']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kabupaten') }}</small>
                </div>
            </div>

            {{--  PROVINSI  --}}
            <div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
                {!! Form::label('provinsi', '* Provinsi', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('provinsi', null, ['class' => 'form-control','placeholder'=>'Provinsi']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('provinsi') }}</small>
                </div>
            </div>

            {{--  KODE POS  --}}
            <div class="form-group{{ $errors->has('kode_pos') ? ' has-error' : '' }}">
                {!! Form::label('kode_pos', 'Kode Pos', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('kode_pos', null, ['class' => 'form-control','placeholder'=>'Kode Pos']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kode_pos') }}</small>
                </div>
            </div>

            {{--  TELP  --}}
            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                {!! Form::label('telp', 'Telp / HP', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('telp', null, ['class' => 'form-control','placeholder'=>'Telp / HP']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('telp') }}</small>
                </div>
            </div>

            {{--  PHOTO  --}}
            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                {!! Form::label('photo', 'Foto', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('photo') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('photo') }}</small>
                    <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>dan Resolusi Gambar Square ex: 512 x 512</p>
                </div>
            </div>

            {{--  TAHUN  --}}
            <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">
            
            {{--  HAPUS  --}}
            <input type="hidden" name="hapus" value="0">
        </div>
    </div>
</div>

{{--  ORTU WALI  --}}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Data Orang Tua / Wali</h3>
    </div>
    <div class="box-body">
        {{--  DATA AYAH  --}}
        <div class="col-md-4">
            
            <div class="form-group{{ $errors->has('ayah_nama') ? ' has-error' : '' }}">
                {!! Form::label('ayah_nama', 'Nama Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::text('ayah_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Ayah']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <small class="text-danger">{{ $errors->first('ayah_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_tl') ? ' has-error' : '' }}">
                {!! Form::label('ayah_tl', 'Tahun Lahir', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ayah_tl', null, ['class' => 'form-control','placeholder'=>'Tahun Lahir Ayah']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_tl') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_pendidikan', 'Pendidikan Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ayah_pendidikan',$pendidikan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_pekerjaan', 'Pekerjaan Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ayah_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_penghasilan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_penghasilan', 'Penghasilan Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ayah_penghasilan',$penghasilan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_penghasilan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_telp') ? ' has-error' : '' }}">
                {!! Form::label('ayah_telp', 'Telp / HP', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ayah_telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telp / HP']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_telp') }}</small>
                </div>
            </div>

        </div>

        {{--  DATA IBU  --}}
        <div class="col-md-4">

            <div class="form-group{{ $errors->has('ibu_nama') ? ' has-error' : '' }}">
                {!! Form::label('ibu_nama', 'Nama Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::text('ibu_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Ibu']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <small class="text-danger">{{ $errors->first('ibu_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_tl') ? ' has-error' : '' }}">
                {!! Form::label('ibu_tl', 'Tahun Lahir', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ibu_tl', null, ['class' => 'form-control','placeholder'=>'Tahun Lahir Ibu']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_tl') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_pendidikan', 'Pendidikan Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ibu_pendidikan',$pendidikan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_pekerjaan', 'Pekerjaan Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ibu_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_penghasilan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_penghasilan', 'Penghasilan Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ibu_penghasilan',$penghasilan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_penghasilan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_telp') ? ' has-error' : '' }}">
                {!! Form::label('ibu_telp', 'Telp / HP', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ibu_telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telp / HP']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_telp') }}</small>
                </div>
            </div>

        </div>

        {{--  DATA WALI  --}}
        <div class="col-md-4">

            <div class="form-group{{ $errors->has('wali_nama') ? ' has-error' : '' }}">
                {!! Form::label('wali_nama', 'Nama Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::text('wali_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Wali']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <small class="text-danger">{{ $errors->first('wali_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_tl') ? ' has-error' : '' }}">
                {!! Form::label('wali_tl', 'Tahun Lahir', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('wali_tl', null, ['class' => 'form-control','placeholder'=>'Tahun Lahir Wali']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_tl') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('wali_pendidikan', 'Pendidikan Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('wali_pendidikan',$pendidikan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('wali_pekerjaan', 'Pekerjaan Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('wali_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_penghasilan') ? ' has-error' : '' }}">
                {!! Form::label('wali_penghasilan', 'Penghasilan Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('wali_penghasilan',$penghasilan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_penghasilan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_telp') ? ' has-error' : '' }}">
                {!! Form::label('wali_telp', 'Telp / HP', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('wali_telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telp / HP']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_telp') }}</small>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
