<div class="col-sm-6">
    <div class="form-group{{ $errors->has('guru_id') ? ' has-error' : '' }}">
        {!! Form::label('guru_id', 'Guru', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('guru_id',$guru ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('guru_id') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('mata_pelajaran_id') ? ' has-error' : '' }}">
        {!! Form::label('mata_pelajaran_id', 'Mata Pelajaran', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          <select class="form-control" name="mata_pelajaran_id">
              @foreach($mapel as $item)
                <option value="{{$item->id}}">{{$item->kode_mat_pel}} - {{$item->mata_pelajaran}}</option>
              @endforeach
          </select>
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('mata_pelajaran_id') }}</small>
        </div>
    </div>

    <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
