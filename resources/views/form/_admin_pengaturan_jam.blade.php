<div class="col-sm-12">
    <div class="form-group{{ $errors->has('jam') ? ' has-error' : '' }}">
        {!! Form::label('jam', 'Jam', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('jam', null, ['class' => 'form-control','placeholder'=>'Jam']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jam') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
