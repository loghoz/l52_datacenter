<div class="col-sm-12">
    <div class="form-group{{ $errors->has('agama') ? ' has-error' : '' }}">
        {!! Form::label('agama', 'Agama', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('agama', null, ['class' => 'form-control','placeholder'=>'Agama']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('agama') }}</small>
        </div>
    </div>

    <input type="hidden" name="tipe" value="agama">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
