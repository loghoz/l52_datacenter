<div class="col-sm-12">
    <div class="col-sm-2">
        <strong>Nama<br>Kelas<br>Hari</strong>
    </div>
    <div class="col-sm-2">
        : {{ $absen->siswa->nama }}<br>
        : {{ $absen->kelas->kode_kelas }}<br>
        : {{ $absen->hari->hari }}<br>
    </div>
    <div class="col-sm-2">
        <strong>Guru<br>Mata Pelajaran<br>Jam</strong>
    </div>
    <div class="col-sm-2">
        : {{ $absen->jadwal->guru->nama }}<br>
        : {{ $absen->jadwal->matpel->mata_pelajaran }}<br>
        : {{ $absen->jadwal->jam_mulai }} - {{ $absen->jadwal->jam_selesai }}<br>
    </div>
</div>
<div class="col-sm-12"><hr></div>
<div class="col-sm-12">
    {{--  ABSENSI  --}}
    <div class="form-group{{ $errors->has('absensi') ? ' has-error' : '' }}">
        {!! Form::label('absensi', 'Absensi : ', ['class'=>'control-label col-sm-2']) !!}
        <div class="col-sm-2">
            <label class="radio-inline">
                @if ($absen->absensi == 'H')
                    {!! Form::radio('absensi', 'H',true) !!} Hadir
                @else
                    {!! Form::radio('absensi', 'H') !!} Hadir
                @endif
            </label>
        </div>
        <div class="col-sm-2">
            <label class="radio-inline">
                @if ($absen->absensi == 'I')
                    {!! Form::radio('absensi', 'I',true) !!} Ijin
                @else
                    {!! Form::radio('absensi', 'I') !!} Ijin
                @endif
            </label>
        </div>
        <div class="col-sm-2">
            <label class="radio-inline">
                @if ($absen->absensi == 'S')
                    {!! Form::radio('absensi', 'S',true) !!} Sakit
                @else
                    {!! Form::radio('absensi', 'S') !!} Sakit
                @endif
            </label>
        </div>
        <div class="col-sm-2">
            <label class="radio-inline">
                @if ($absen->absensi == 'A')
                    {!! Form::radio('absensi', 'A',true) !!} Alpa
                @else
                    {!! Form::radio('absensi', 'A') !!} Alpa
                @endif
            </label>
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
            <small class="text-danger">{{ $errors->first('absensi') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
