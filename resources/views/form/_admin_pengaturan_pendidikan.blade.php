<div class="col-sm-12">

    <div class="form-group{{ $errors->has('pendidikan') ? ' has-error' : '' }}">
        {!! Form::label('pendidikan', 'Pendidikan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('pendidikan', null, ['class' => 'form-control','placeholder'=>'Pendidikan']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('pendidikan') }}</small>
        </div>
    </div>

    <input type="hidden" name="tipe" value="pendidikan">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
