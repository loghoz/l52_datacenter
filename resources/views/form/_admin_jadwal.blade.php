<div class="col-sm-12">
    <div class="col-sm-6">
        <div class="form-group{{ $errors->has('guru_id') ? ' has-error' : '' }}">
            {!! Form::label('guru_id', 'Guru', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::select('guru_id',$guru ,null,['class'=>'form-control','id'=>'guru']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('guru_id') }}</small>
            </div>
        </div>

        <div class="form-group{{ $errors->has('mata_pelajaran_id') ? ' has-error' : '' }}">
            {!! Form::label('mata_pelajaran_id', 'Mata Pelajaran', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::select('mata_pelajaran_id',['placeholder'=>'Pilih Guru'],null,['class'=>'form-control','id'=>'pemapel']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('mata_pelajaran_id') }}</small>
            </div>
        </div>

        <div class="form-group{{ $errors->has('kode_kelas') ? ' has-error' : '' }}">
            {!! Form::label('kode_kelas', 'Kelas', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::select('kode_kelas',$kelas ,null,['class'=>'form-control']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('kode_kelas') }}</small>
            </div>
        </div>

        <div class="form-group{{ $errors->has('hari_id') ? ' has-error' : '' }}">
            {!! Form::label('hari_id', 'Hari', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::select('hari_id',$hari ,null,['class'=>'form-control']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('hari_id') }}</small>
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group{{ $errors->has('jam_mulai') ? ' has-error' : '' }}">
            {!! Form::label('jam_mulai', 'Jam Mulai', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::select('jam_mulai',$jam ,null,['class'=>'form-control']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('jam_mulai') }}</small>
            </div>
        </div>

        <div class="form-group{{ $errors->has('jam_selesai') ? ' has-error' : '' }}">
            {!! Form::label('jam_selesai', 'Jam Selesai', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
                {!! Form::select('jam_selesai',$jam ,null,['class'=>'form-control']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('jam_selesai') }}</small>
            </div>
        </div>

        <div class="form-group{{ $errors->has('jam_ajar') ? ' has-error' : '' }}">
            {!! Form::label('jam_ajar', 'Jam Ajar', ['class'=>'control-label col-sm-4']) !!}
            <div class="col-sm-8">
              {!! Form::text('jam_ajar', null, ['class' => 'form-control','placeholder'=>'... Jam']) !!}
            </div>
            <div class="col-sm-4"></div>
            <div class="col-sm-8">
              <small class="text-danger">{{ $errors->first('jam_ajar') }}</small>
            </div>
        </div>

        <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">

        <div class="btn-group pull-right">
            {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
            {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
        </div>
  </div>
</div>
