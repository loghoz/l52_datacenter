<div class="box box-success">
    <div class="box-body">
        <div class="col-md-4">

            <div class="form-group{{ $errors->has('ayah_nama') ? ' has-error' : '' }}">
                {!! Form::label('ayah_nama', 'Nama Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::text('ayah_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Ayah']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <small class="text-danger">{{ $errors->first('ayah_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_tl') ? ' has-error' : '' }}">
                {!! Form::label('ayah_tl', 'Tahun Lahir', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ayah_tl', null, ['class' => 'form-control','placeholder'=>'Tahun Lahir Ayah']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_tl') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_pendidikan', 'Pendidikan Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ayah_pendidikan',$pendidikan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_pekerjaan', 'Pekerjaan Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ayah_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_penghasilan') ? ' has-error' : '' }}">
                {!! Form::label('ayah_penghasilan', 'Penghasilan Ayah', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ayah_penghasilan',$penghasilan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_penghasilan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ayah_telp') ? ' has-error' : '' }}">
                {!! Form::label('ayah_telp', 'Telp / HP', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ayah_telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telp / HP']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ayah_telp') }}</small>
                </div>
            </div>

        </div>
        <div class="col-md-4">

            <div class="form-group{{ $errors->has('ibu_nama') ? ' has-error' : '' }}">
                {!! Form::label('ibu_nama', 'Nama Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::text('ibu_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Ibu']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <small class="text-danger">{{ $errors->first('ibu_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_tl') ? ' has-error' : '' }}">
                {!! Form::label('ibu_tl', 'Tahun Lahir', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ibu_tl', null, ['class' => 'form-control','placeholder'=>'Tahun Lahir Ibu']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_tl') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_pendidikan', 'Pendidikan Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ibu_pendidikan',$pendidikan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_pekerjaan', 'Pekerjaan Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ibu_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_penghasilan') ? ' has-error' : '' }}">
                {!! Form::label('ibu_penghasilan', 'Penghasilan Ibu', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('ibu_penghasilan',$penghasilan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_penghasilan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('ibu_telp') ? ' has-error' : '' }}">
                {!! Form::label('ibu_telp', 'Telp / HP', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('ibu_telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telp / HP']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('ibu_telp') }}</small>
                </div>
            </div>

        </div>
        <div class="col-md-4">

            <div class="form-group{{ $errors->has('wali_nama') ? ' has-error' : '' }}">
                {!! Form::label('wali_nama', 'Nama Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::text('wali_nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap Wali']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                    <small class="text-danger">{{ $errors->first('wali_nama') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_tl') ? ' has-error' : '' }}">
                {!! Form::label('wali_tl', 'Tahun Lahir', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('wali_tl', null, ['class' => 'form-control','placeholder'=>'Tahun Lahir Wali']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_tl') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_pendidikan') ? ' has-error' : '' }}">
                {!! Form::label('wali_pendidikan', 'Pendidikan Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('wali_pendidikan',$pendidikan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_pendidikan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_pekerjaan') ? ' has-error' : '' }}">
                {!! Form::label('wali_pekerjaan', 'Pekerjaan Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('wali_pekerjaan',$pekerjaan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_pekerjaan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_penghasilan') ? ' has-error' : '' }}">
                {!! Form::label('wali_penghasilan', 'Penghasilan Wali', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                    {!! Form::select('wali_penghasilan',$penghasilan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_penghasilan') }}</small>
                </div>
            </div>

            <div class="form-group{{ $errors->has('wali_telp') ? ' has-error' : '' }}">
                {!! Form::label('wali_telp', 'Telp / HP', ['class'=>'control-label col-sm-5']) !!}
                <div class="col-sm-7">
                  {!! Form::text('wali_telp', null, ['class' => 'form-control','placeholder'=>'Nomor Telp / HP']) !!}
                </div>
                <div class="col-sm-5"></div>
                <div class="col-sm-7">
                  <small class="text-danger">{{ $errors->first('wali_telp') }}</small>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
