<div class="col-sm-12">

    <div class="form-group{{ $errors->has('ujian') ? ' has-error' : '' }}">
        {!! Form::label('ujian', 'Ujian', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('ujian', null, ['class' => 'form-control','placeholder'=>'Ujian']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('ujian') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('jumlah_soal') ? ' has-error' : '' }}">
        {!! Form::label('jumlah_soal', 'Jumlah', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('jumlah_soal', null, ['class' => 'form-control','placeholder'=>'Jumlah Soal']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jumlah_soal') }}</small>
        </div>
    </div>

    <input type="hidden" name="tipe" value="jumlah_soal">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
