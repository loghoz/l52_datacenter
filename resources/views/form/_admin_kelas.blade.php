<div class="col-sm-6">

    <div class="form-group{{ $errors->has('tingkat') ? ' has-error' : '' }}">
        {!! Form::label('tingkat', 'Tingkat', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-3">
            <label class="radio-inline">
                {!! Form::radio('tingkat', '10') !!} 10
            </label>
        </div>
        <div class="col-sm-3">
            <label class="radio-inline">
                {!! Form::radio('tingkat', '11') !!} 11
            </label>
        </div>
        <div class="col-sm-2">
            <label class="radio-inline">
                {!! Form::radio('tingkat', '12') !!} 12
            </label>
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('tingkat') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('jurusan_id') ? ' has-error' : '' }}">
        {!! Form::label('jurusan_id', 'Jurusan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('jurusan_id',$jurusan ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jurusan_id') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('kode_kelas') ? ' has-error' : '' }}">
        {!! Form::label('kode_kelas', 'Kode Kelas', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('kode_kelas', null, ['class' => 'form-control','placeholder'=>'Contoh kode kelas : 10-AK3']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('kode_kelas') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('aktif') ? ' has-error' : '' }}">
        {!! Form::label('aktif', 'Aktif', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::select('aktif',array('Aktif'=>'Aktif','Tidak Aktif'=>'Tidak Aktif') ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('aktif') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
