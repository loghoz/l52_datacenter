<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">Ubah Data Guru</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">

            {{--  GURU  --}}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {!! Form::label('nama', '* Nama Lengkap', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('nama', null, ['class' => 'form-control','placeholder'=>'Nama Lengkap']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('nama') }}</small>
                </div>
            </div>

            {{--  JK  --}}
            <div class="form-group{{ $errors->has('jk') ? ' has-error' : '' }}">
                {!! Form::label('jk', '* Jeno_induk Kelamin', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-4">
                    <label class="radio-inline">
                        @if ($guru->guru_meta->jk == 'laki-laki')
                            {!! Form::radio('jk', 'laki-laki',true) !!} Laki - Laki
                        @else
                            {!! Form::radio('jk', 'laki-laki') !!} Laki - Laki
                        @endif
                    </label>
                </div>
                <div class="col-sm-4">
                    <label class="radio-inline">
                        @if ($guru->guru_meta->jk == 'perempuan')
                            {!! Form::radio('jk', 'perempuan',true) !!} Perempuan
                        @else
                            {!! Form::radio('jk', 'perempuan') !!} Perempuan
                        @endif
                    </label>
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('jk') }}</small>
                </div>
            </div>

            {{--  TEMPAT LAHIR  --}}
            <div class="form-group{{ $errors->has('tempat_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tempat_lahir', '* Tempat Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('tempat_lahir', ( isset($guru->guru_meta->tempat_lahir) ? $guru->guru_meta->tempat_lahir : null ), ['class' => 'form-control','placeholder'=>'Tempat Lahir']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tempat_lahir') }}</small>
                </div>
            </div>

            {{--  TANGGAL LAHIR  --}}
            <div class="form-group{{ $errors->has('tanggal_lahir') ? ' has-error' : '' }}">
                {!! Form::label('tanggal_lahir', '* Tanggal Lahir', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::date('tanggal_lahir', ( isset($guru->guru_meta->tanggal_lahir) ? $guru->guru_meta->tanggal_lahir : null ), ['class' => 'form-control']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('tanggal_lahir') }}</small>
                </div>
            </div>

            {{--  AGAMA  --}}
            <div class="form-group{{ $errors->has('agama_id') ? ' has-error' : '' }}">
                {!! Form::label('agama_id', '* Agama', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('agama_id',$agama ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('agama_id') }}</small>
                </div>
            </div>

            {{--  NO INDUK  --}}
            <div class="form-group{{ $errors->has('no_induk') ? ' has-error' : '' }}">
                {!! Form::label('no_induk', '* NIG', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('no_induk', null, ['class' => 'form-control','placeholder'=>'No Induk Guru']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('no_induk') }}</small>
                </div>
            </div>

            {{--  NUPTK  --}}
            <div class="form-group{{ $errors->has('nuptk') ? ' has-error' : '' }}">
                {!! Form::label('nuptk', 'NUPTK', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('nuptk', ( isset($guru->guru_meta->nuptk) ? $guru->guru_meta->nuptk : null ), ['class' => 'form-control','placeholder'=>'NUPTK']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('nuptk') }}</small>
                </div>
            </div>

            {{--  TAHUN TUGAS  --}}
            <div class="form-group{{ $errors->has('tahun_tugas') ? ' has-error' : '' }}">
                {!! Form::label('tahun_tugas', '* Tahun Tugas', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('tahun_tugas', ( isset($guru->guru_meta->tahun_tugas) ? $guru->guru_meta->tahun_tugas : null ), ['class' => 'form-control','placeholder'=>'Tahun Tugas']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('tahun_tugas') }}</small>
                </div>
            </div>

            {{--  STATUS  --}}
            <div class="form-group{{ $errors->has('status_id') ? ' has-error' : '' }}">
                {!! Form::label('status_id', '* Status', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('status_id',$status ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('status_id') }}</small>
                </div>
            </div>

            {{--  JABATAN  --}}
            <div class="form-group{{ $errors->has('jabatan_id') ? ' has-error' : '' }}">
                {!! Form::label('jabatan_id', '* Jabatan', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::select('jabatan_id',$jabatan ,null,array('class'=>'form-control')) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('jabatan_id') }}</small>
                </div>
            </div>

            {{--  IJAZAH JURUSAN  --}}
            <div class="form-group{{ $errors->has('ijazah_jurusan') ? ' has-error' : '' }}">
                {!! Form::label('ijazah_id', '* Ijazah', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-3">
                    {!! Form::select('ijazah_id',$ijazah ,null,array('class'=>'form-control col-sm-4')) !!}
                </div>
                <div class="col-sm-5">
                    {!! Form::text('ijazah_jurusan', ( isset($guru->guru_meta->ijazah_jurusan) ? $guru->guru_meta->ijazah_jurusan : null ), ['class' => 'form-control col-sm-8','placeholder'=>'Ijazah']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('ijazah_jurusan') }}</small>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            {{--  ALAMAT  --}}
            <div class="form-group{{ $errors->has('alamat') ? ' has-error' : '' }}">
                {!! Form::label('alamat', '* Alamat', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('alamat', ( isset($guru->guru_meta->alamat) ? $guru->guru_meta->alamat : null ), ['class' => 'form-control','placeholder'=>'Alamat']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('alamat') }}</small>
                </div>
            </div>

            {{--  RT  --}}
            <div class="form-group{{ $errors->has('rt') ? ' has-error' : '' }}">
                {!! Form::label('rt', '* RT', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('rt', ( isset($guru->guru_meta->rt) ? $guru->guru_meta->rt : null ), ['class' => 'form-control','placeholder'=>'RT']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('rt') }}</small>
                </div>
            </div>

            {{--  RW  --}}
            <div class="form-group{{ $errors->has('rw') ? ' has-error' : '' }}">
                {!! Form::label('rw', '* RW', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::text('rw', ( isset($guru->guru_meta->rw) ? $guru->guru_meta->rw : null ), ['class' => 'form-control','placeholder'=>'RW']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('rw') }}</small>
                </div>
            </div>

            {{--  DESA  --}}
            <div class="form-group{{ $errors->has('desa') ? ' has-error' : '' }}">
                {!! Form::label('desa', '* Dusun / Desa', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('desa', ( isset($guru->guru_meta->desa) ? $guru->guru_meta->desa : null ), ['class' => 'form-control','placeholder'=>'Dusun / Desa']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('desa') }}</small>
                </div>
            </div>

            {{--  KECAMATAN  --}}
            <div class="form-group{{ $errors->has('kecamatan') ? ' has-error' : '' }}">
                {!! Form::label('kecamatan', '* Kecamatan', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('kecamatan', ( isset($guru->guru_meta->kecamatan) ? $guru->guru_meta->kecamatan : null ), ['class' => 'form-control','placeholder'=>'Kecamatan']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kecamatan') }}</small>
                </div>
            </div>

            {{--  KABUPATEN  --}}
            <div class="form-group{{ $errors->has('kabupaten') ? ' has-error' : '' }}">
                {!! Form::label('kabupaten', '* Kabupaten / Kota', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('kabupaten', ( isset($guru->guru_meta->kabupaten) ? $guru->guru_meta->kabupaten : null ), ['class' => 'form-control','placeholder'=>'Kabupaten / Kota']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kabupaten') }}</small>
                </div>
            </div>

            {{--  PROVINSI  --}}
            <div class="form-group{{ $errors->has('provinsi') ? ' has-error' : '' }}">
                {!! Form::label('provinsi', '* Provinsi', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('provinsi', ( isset($guru->guru_meta->provinsi) ? $guru->guru_meta->provinsi : null ), ['class' => 'form-control','placeholder'=>'Provinsi']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('provinsi') }}</small>
                </div>
            </div>

            {{--  KODE POS  --}}
            <div class="form-group{{ $errors->has('kode_pos') ? ' has-error' : '' }}">
                {!! Form::label('kode_pos', 'Kode Pos', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('kode_pos', ( isset($guru->guru_meta->kode_pos) ? $guru->guru_meta->kode_pos : null ), ['class' => 'form-control','placeholder'=>'Kode Pos']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('kode_pos') }}</small>
                </div>
            </div>

            {{--  TELP  --}}
            <div class="form-group{{ $errors->has('telp') ? ' has-error' : '' }}">
                {!! Form::label('telp', 'Telp / HP', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                  {!! Form::text('telp', ( isset($guru->guru_meta->telp) ? $guru->guru_meta->telp : null ), ['class' => 'form-control','placeholder'=>'Telp / HP']) !!}
                </div>
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                  <small class="text-danger">{{ $errors->first('telp') }}</small>
                </div>
            </div>

            {{--  PHOTO  --}}
            <div class="form-group{{ $errors->has('photo') ? ' has-error' : '' }}">
                {!! Form::label('photo', 'Foto', ['class'=>'control-label col-sm-4']) !!}
                <div class="col-sm-8">
                    {!! Form::file('photo') !!}
                </div>
                <div class="col-sm-8">
                    <small class="text-danger">{{ $errors->first('photo') }}</small>
                    <p>Pastikan Ukuran Gambar Tidak Lebih dari 2 MB <br>dan Resolusi Gambar Square ex: 512 x 512</p>
                </div>
                @if (isset($guru) && $guru->photo !== '')
                    <div class="row">
                        <div class="col-md-6">
                            <br><p>Foto Sebelumnya:</p>
                            <div class="thumbnail">
                                <img src="{{ url('img/guru/' . $guru->guru_meta->photo) }}" class="img-rounded">
                            </div>
                        </div>
                    </div>
                @endif
            </div>

            {{--  TAHUN  --}}
            <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">

            {{--  HAPUS  --}}
            <input type="hidden" name="hapus" value="0">
        </div>
    </div>
</div>
<div class="btn-group pull-right">
    {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
    {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
</div>
