<div class="col-sm-6">

    <div class="form-group{{ $errors->has('no_induk') ? ' has-error' : '' }}">
        {!! Form::label('no_induk', 'No Induk', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::text('no_induk', null, ['class' => 'form-control','placeholder'=>'ex = 123456']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('no_induk') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
        {!! Form::label('username', 'Username', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::text('username', null, ['class' => 'form-control','placeholder'=>'Username']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('username') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        {!! Form::label('password', 'Password', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('password', null, ['class' => 'form-control','placeholder'=>'Password']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('password') }}</small>
        </div>
    </div>

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
