<div class="col-sm-6">
    <div class="form-group{{ $errors->has('kode_mat_pel') ? ' has-error' : '' }}">
        {!! Form::label('kode_mat_pel', 'Kode Mata Pelajaran', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('kode_mat_pel', null, ['class' => 'form-control','placeholder'=>'Contoh : 01NA ( NA = Normatif Adaptif )']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('kode_mat_pel') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('mata_pelajaran') ? ' has-error' : '' }}">
        {!! Form::label('mata_pelajaran', 'Mata Pelajaran', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('mata_pelajaran', null, ['class' => 'form-control','placeholder'=>'Mata Pelajaran']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('mata_pelajaran') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('deskripsi') ? ' has-error' : '' }}">
        {!! Form::label('deskripsi', 'Deskripsi', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('deskripsi', null, ['class' => 'form-control','placeholder'=>'Deskripsi mata pelajaran']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
        </div>
    </div>

    <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">
    <input type="hidden" name="jenis_mat_pel" value="Normatif dan Adaptif">
    <input type="hidden" name="jurusan_id" value="1">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
