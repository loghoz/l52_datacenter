<div class="col-sm-12">

    <div class="form-group{{ $errors->has('penghasilan') ? ' has-error' : '' }}">
        {!! Form::label('penghasilan', 'Penghasilan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('penghasilan', null, ['class' => 'form-control','placeholder'=>'Penghasilan']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('penghasilan') }}</small>
        </div>
    </div>

    <input type="hidden" name="tipe" value="penghasilan">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
