<div class="col-sm-12">
    <div class="form-group{{ $errors->has('jabatan') ? ' has-error' : '' }}">
        {!! Form::label('jabatan', 'Jabatan', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
          {!! Form::text('jabatan', null, ['class' => 'form-control','placeholder'=>'Jabatan']) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('jabatan') }}</small>
        </div>
    </div>

    <input type="hidden" name="tipe" value="jabatan">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
