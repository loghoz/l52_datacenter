<div class="col-sm-6">
    <div class="form-group{{ $errors->has('guru_id') ? ' has-error' : '' }}">
        {!! Form::label('guru_id', 'Guru', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('guru_id',$guru ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('guru_id') }}</small>
        </div>
    </div>

    <div class="form-group{{ $errors->has('kelas_id') ? ' has-error' : '' }}">
        {!! Form::label('kelas_id', 'Kelas', ['class'=>'control-label col-sm-4']) !!}
        <div class="col-sm-8">
            {!! Form::select('kelas_id',$kelas ,null,array('class'=>'form-control')) !!}
        </div>
        <div class="col-sm-4"></div>
        <div class="col-sm-8">
          <small class="text-danger">{{ $errors->first('kelas_id') }}</small>
        </div>
    </div>

    <input type="hidden" name="tahun_ajaran" value="{{ Cookie::get('tahun_ajaran') }}">

    <div class="btn-group pull-right">
        {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
        {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
