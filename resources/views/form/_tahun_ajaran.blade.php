<div class="col-sm-12">
    <div class="form-group{{ $errors->has('tahun_ajaran') ? ' has-error' : '' }}">
        {!! Form::select('tahun_ajaran',$tahun ,null,array('class'=>'form-control has-feedback')) !!}
        <small class="text-danger">{{ $errors->first('tahun_ajaran') }}</small>
    </div>

    <div class="btn-group pull-right">
        {!! Form::submit("Pilih", ['class' => 'btn btn-primary']) !!}
    </div>
</div>
