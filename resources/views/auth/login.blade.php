<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Login</title>

    <!-- Styles -->
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">

    <style>
        .bg {
            font-family: 'Lato';
            background-image: url('{{ asset ('img/bg-login.jpg') }}');
            background-repeat: no-repeat;
            position: fixed;
            width: 100%;
            height: 100%;
            background-size: 100%;
        }

        .shadow{
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

<body class="bg hold-transition login-page">
  <div class="login-box">
  <div class="login-logo">
    <div align="center" style="padding-top:10px;">
        <img src="{{asset('img/logo.png')}}" width="150px" height="150px" alt="logo pelita" />
    </div>
    <font color="#ffffff"><b>DATA</b>Center</font>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body shadow">
    <p class="login-box-msg">LOGIN</p>

    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
            <label for="username" class="col-md-4 control-label">Username</label>

            <div class="col-md-8 has-feedback">
                <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if ($errors->has('username'))
                    <span class="help-block">
                        <strong>{{ $errors->first('username') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Password</label>

            <div class="col-md-8 has-feedback">
                <input id="password" type="password" class="form-control" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group" align="right">
            <div class="col-md-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-btn fa-sign-in"></i> Login
                </button>
            </div>
        </div>
    </form>
  </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/adminlte.js') }}"></script>

</body>
</html>
