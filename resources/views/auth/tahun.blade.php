<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SMK Pelita</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">

    <style>
        .bg {
            font-family: 'Lato';
            background-image: url('{{ asset ('img/bg-login.jpg') }}');
            background-repeat: no-repeat;
            position: fixed;
            width: 100%;
            height: 100%;
            background-size: 100%;
        }

        .shadow{
          box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);;
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>

<body class="bg hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <div align="center">
          <img src="{{asset('img/logo.png')}}" width="150px" height="150px" alt="logo pelita" />
      </div>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body shadow">
      <p class="login-box-msg">Pilih Tahun Ajaran</p>
      {!! Form::open(['route' => 'tahun.store','class'=>'form-horizontal'])!!}
            <div class="form-group{{ $errors->has('tahun_ajaran') ? ' has-error' : '' }}">
                {!! Form::select('tahun_ajaran',$ta ,null,array('class'=>'form-control has-feedback')) !!}
                <small class="text-danger">{{ $errors->first('tahun_ajaran') }}</small>
            </div>

            <div class="form-group" align="right">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-sign-in"></i> Pilih
                    </button>
                </div>
            </div>
            @if(session()->has('message'))
                <div class="alert alert-danger">
                    {{ session()->get('message') }}
                </div>
            @endif
        {!! Form::close() !!}
    </div>
  </div>

  <script src="{{ asset('js/adminlte.js') }}"></script>

</body>
</html>