@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Mata Pelajaran Produktif
        </h1>
    </section>

    <section class="content">
        <div class="row">
              <div class="col-md-12" align="right">
                <a href="{{ url('/admin/mapel/na') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Normatif Adaptive</a>
                <a href="{{ url('/admin/mapel/pemapel') }}" class="btn btn-success"><span class="fa fa-eye"></span> Pembagian Mata Pelajaran</a>
                <br><br>
              </div>
              <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Mata Pelajaran</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.mapel.pro.store', 'class'=>'form-horizontal'])!!}
                            @include('form._admin_mapel_pro')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Mata Pelajaran</h3>
                    </div>
                    <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td>No</td>
                                  <td>Kode Mata Pelajaran</td>
                                  <td>Jurusan</td>
                                  <td>Mata Pelajaran</td>
                                  <td>Deskripsi</td>
                                  <td align="center">Action</td>
                              </tr>
                            </thead>
                            <tbody>

                            @foreach($mapel as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->kode_mat_pel }}</td>
                                    <td>{{ $item->jurusan->jurusan }}</td>
                                    <td>{{ $item->mata_pelajaran }}</td>
                                    <td>{{ $item->deskripsi }}</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.mapel.pro.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model($item, ['route' => ['admin.mapel.pro.destroy', $item], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $mapel->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
