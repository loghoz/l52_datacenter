@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Normatif dan Adaptif
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Mata Pelajaran</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($mapel, ['route' => ['admin.mapel.na.update', $mapel],'method' =>'patch','class'=>'form-horizontal'])!!}
                        @include('form._admin_mapel_na', ['model' => $mapel])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
