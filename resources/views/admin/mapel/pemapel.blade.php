@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Pembagian Mata Pelajaran
        </h1>
    </section>

    <section class="content">
        <div class="row">
            {{--  MENU  --}}
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/mapel/na') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Normatif Adaptive</a>
                <a href="{{ url('/admin/mapel/pro') }}" class="btn btn-success"><span class="fa fa-eye"></span> Produktif</a>
                <br><br>
            </div>

            <div class="col-md-12">

                {{--  TAMBAH PEMAPEL  --}}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pembagian Mata Pelajaran</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.mapel.pemapel.store', 'class'=>'form-horizontal'])!!}
                            @include('form._admin_pemapel')
                        {!! Form::close() !!}
                    </div>
                </div>

                {{--  DATA PEMAPEL  --}}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Pembagian Mata Pelajaran</h3>
                    </div>
                    <div class="box-body">

                      {!! Form::open(['url' => 'admin/mapel/pemapel', 'method'=>'get', 'class'=>'form-inline'])!!}
                        <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                            {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Cari Guru....']) !!}
                            {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                        </div>
                        {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                      {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td>No</td>
                                  <td>No Induk</td>
                                  <td>Nama</td>
                                  <td align="center">Action</td>
                              </tr>
                            </thead>
                            <tbody>
                            @forelse($pemap as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->no_induk }}</td>
                                    <td>{{ $item->nama }}</td>
                                    <td align="center">
                                        <button class="btn btn-primary" data-toggle="modal" data-target="#detail-{{ $item->id }}">Detail</button>

                                        <div class="modal fade" id="detail-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        <h4 class="modal-title" id="{{ $item->id }}">
                                                            <img class="img-circle" src="{{ url('img/guru/' . $item->guru_meta->photo) }}" width="150" height="150" class="img-circle" alt="User Image"><br>
                                                            {{ $item->nama }}<br>
                                                        </h4>
                                                    </div>

                                                    <div class="modal-body">
                                                        <div class='table-responsive'>
                                                          <table class='table table-striped table-bordered table-hover table-condensed'>
                                                            <thead>
                                                              <tr>
                                                                <th>Kode MatPel</th>
                                                                <th>Mata Pelajaran</th>
                                                                <th>Jurusan</th>
                                                                <th>Aksi</th>
                                                              </tr>
                                                            </thead>
                                                            <tbody>
                                                              @foreach ($item->pemapel as $map)
                                                                @if($map->tahun_ajaran == $tahun_ajaran)
                                                                  <tr>
                                                                    <td>{{ $map->matpel->kode_mat_pel }}</td>
                                                                    <td>{{ $map->matpel->mata_pelajaran }}</td>
                                                                    <td>{{ $map->matpel->jurusan->jurusan}}</td>
                                                                    <td align="center">
                                                                      {{ Form::open(['route' => ['admin.mapel.pemapel.destroy' , $map->id] ,'method' => 'DELETE']) }}
                                                                        {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                                                      {{ Form::close() }}
                                                                    </td>
                                                                  </tr>
                                                                @endif
                                                              @endforeach
                                                            </tbody>
                                                          </table>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                              @empty
                              <div class="col-md-12 text-center">
                                @if (isset($q))
                                  <h1>:(</h1>
                                  <p>Guru tidak ditemukan.</p>
                                @endif
                                <p><a href="{{ url('/admin/guru') }}">Lihat semua data guru <i class="fa fa-arrow-right"></i></a></p>
                              </div>
                            @endforelse
                            </tbody>
                        </table>
                        {{ $pemap->appends(compact('q', 'page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
