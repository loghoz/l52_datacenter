<div class="box box-widget widget-user">
    <div class="widget-user-header bg-aqua-active">
        <h3 class="widget-user-username">{{ $gurus->nama }}</h3>
        <h5 class="widget-user-desc">{{ $gurus->jabatan }}</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="{{ url('img/guru/' . $gurus->photo) }}" alt="User Avatar">
    </div>
    <div class="box-footer">
        <div class="row">

            <div class="col-sm-6 border-right">
                <div class="description-block">
                      {{ Form::open(['url' => ['admin/guru/balikin' ,$gurus->id] ,'method' => 'PUT']) }}
                        {{ Form::submit('Pulihkan', ['class' => 'btn btn-primary kotak-sampah']) }}
                      {{ Form::close() }}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="description-block">
                    {{ Form::open(['route' => ['admin.guru.destroy' ,$gurus->id] ,'method' => 'DELETE']) }}
                      {{ Form::submit('Hapus Permanen', ['class' => 'btn btn-danger hapus-permanen']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
