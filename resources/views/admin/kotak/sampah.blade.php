@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Kotak Sampah
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Siswa</h3>
                    </div>
                    <div class="box-body">
                          @forelse ($siswa as $sis)
                              <div class="col-md-4">
                                  @include('admin.kotak._profil_siswa', ['siswa' => $sis])
                              </div>
                            @empty
                            <div class="col-md-12 text-center">
                              @if (isset($q))
                                <h1>:(</h1>
                                <p>Siswa tidak ditemukan.</p>
                              @endif
                                <p><a href="{{ url('/admin/siswa') }}">Lihat semua data siswa <i class="fa fa-arrow-right"></i></a></p>
                            </div>
                          @endforelse
                          {{ $siswa->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Karyawan</h3>
                    </div>
                    <div class="box-body">
                          @forelse ($guru as $gurus)
                              <div class="col-md-4">
                                  @include('admin.kotak._profil_guru', ['guru' => $gurus])
                              </div>
                            @empty
                            <div class="col-md-12 text-center">
                              @if (isset($q))
                                <h1>:(</h1>
                                <p>Karyawan tidak ditemukan.</p>
                              @endif
                                <p><a href="{{ url('/admin/guru') }}">Lihat semua data Karyawan <i class="fa fa-arrow-right"></i></a></p>
                            </div>
                          @endforelse
                          {{ $guru->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
