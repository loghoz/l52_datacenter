@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Wali Kelas
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/guru') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data Karyawan</a>
                <br><br>
            </div>

            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Wali Kelas</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.walikelas.store', 'class'=>'form-horizontal'])!!}
                            @include('form._admin_walikelas')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Wali Kelas</h3>
                    </div>
                    <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Guru</td>
                                <td>Wali Kelas</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($walikelas as $wakel)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $wakel->guru->nama }}</td>
                                    <td>{{ $wakel->kelas->kode_kelas }}</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.walikelas.edit', $wakel->id) }}" class="btn btn-primary">Ubah</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model($wakel, ['route' => ['admin.walikelas.destroy', $wakel], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $walikelas->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
