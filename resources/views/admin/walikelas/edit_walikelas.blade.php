@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Wali Kelas
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Wali Kelas</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($wali, ['route' => ['admin.walikelas.update', $wali],'method' =>'patch','class'=>'form-horizontal'])!!}
                        @include('form._admin_walikelas', ['model' => $wali])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
