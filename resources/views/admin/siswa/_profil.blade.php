<div class="box box-widget widget-user">
    {{--  HEADER  --}}
    <div class="widget-user-header bg-aqua-active">
        <h3 class="widget-user-username">{{ $item->nama }}</h3>
        <h5 class="widget-user-desc">{{ $item->kelas->kode_kelas }}</h5>
    </div>

    {{--  IMAGE PROFIL  --}}
    <div class="widget-user-image">
        <img class="img-circle" src="{{ url('img/siswa/' . $item->siswa_meta->photo) }}" alt="User Avatar">
    </div>

    {{--  DATA  --}}
    <div class="box-footer">
        <div class="row">

            {{--  DATA WALI  --}}
            <div class="col-sm-4 border-right">
                <div class="description-block">
                    <button class="btn btn-success" data-toggle="modal" data-target="#ortu-{{ $item->id }}">Wali</button>

                    <div class="modal modal-success fade" id="ortu-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="{{ $item->id }}">
                                        <img class="img-circle" src="{{ url('img/siswa/' . $item->siswa_meta->photo) }}" width="150" height="150" class="img-circle" alt="User Image"><br>
                                        Orang Tua / Wali<br>
                                        {{ $item->nama }}
                                    </h4>
                                </div>

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <strong>Label</strong><br>
                                            Nama<br>
                                            Tahun Lahir<br>
                                            Pendidikan<br>
                                            Pekerjaan<br>
                                            Penghasilan<br>
                                            Telp / HP
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Ayah</strong><br>
                                            {{ $item->siswa_wali->ayah_nama }}<br>
                                            {{ $item->siswa_wali->ayah_tl }}<br>
                                            {{ $item->siswa_wali->ayah_pendidikan }}<br>
                                            {{ $item->siswa_wali->ayah_pekerjaan }}<br>
                                            {{ $item->siswa_wali->ayah_penghasilan }}<br>
                                            {{ $item->siswa_wali->ayah_telp }}
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Ibu</strong><br>
                                            {{ $item->siswa_wali->ibu_nama }}<br>
                                            {{ $item->siswa_wali->ibu_tl }}<br>
                                            {{ $item->siswa_wali->ibu_pendidikan }}<br>
                                            {{ $item->siswa_wali->ibu_pekerjaan }}<br>
                                            {{ $item->siswa_wali->ibu_penghasilan }}<br>
                                            {{ $item->siswa_wali->ibu_telp }}
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Wali</strong><br>
                                            {{ $item->siswa_wali->wali_nama }}<br>
                                            {{ $item->siswa_wali->wali_tl }}<br>
                                            {{ $item->siswa_wali->wali_pendidikan }}<br>
                                            {{ $item->siswa_wali->wali_pekerjaan }}<br>
                                            {{ $item->siswa_wali->wali_penghasilan }}<br>
                                            {{ $item->siswa_wali->wali_telp }}
                                        </div>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button onclick="window.location.href='{{ route('admin.siswawali.edit', $item->id) }}'" class="btn btn-default">Ubah</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{--  DATA SISWA  --}}
            <div class="col-sm-4 border-right">
                <div class="description-block">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#detail-{{ $item->id }}">Detail</button>

                    <div class="modal modal-primary fade" id="detail-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="{{ $item->id }}">
                                        <img class="img-circle" src="{{ url('img/siswa/' . $item->siswa_meta->photo) }}" width="150" height="150" class="img-circle" alt="User Image"><br>
                                        {{ $item->nama }}<br>
                                        {{ $item->kelas->kode_kelas }}
                                    </h4>
                                </div>

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <strong>Agama<br>
                                            NIS<br>
                                            Tempat lahir</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $item->agama->agama }}<br>
                                            {{ $item->no_induk }}<br>
                                            {{ $item->siswa_meta->tempat_lahir }}
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Jenis Kelamin<br>
                                            NISN<br>
                                            Tanggal Lahir</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $item->siswa_meta->jk }}<br>
                                            {{ $item->siswa_meta->nisn }}<br>
                                            {{ $item->siswa_meta->tanggal_lahir }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br>Alamat : {{ $item->siswa_meta->alamat }}, RT / RW : {{ $item->siswa_meta->rt }} / {{ $item->siswa_meta->rw }}<br><br>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <strong>Dusun / Desa<br>
                                            Kecamatan</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $item->siswa_meta->desa }}<br>
                                            {{ $item->siswa_meta->kecamatan }}
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Kabupaten / Kota<br>
                                            Provinsi</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $item->siswa_meta->kabupaten }}<br>
                                            {{ $item->siswa_meta->provinsi }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br>Kode POS : {{ $item->siswa_meta->kode_pos }} <=> Telp : {{ $item->siswa_meta->telp }}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button onclick="window.location.href='{{ route('admin.siswa.edit', $item->id) }}'" class="btn btn-default">Ubah</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{--  HAPUS SISWA  --}}
            <div class="col-sm-4">
                <div class="description-block">
                    {{ Form::open(['url' => ['admin/siswa/hapus' ,$item->id] ,'method' => 'PUT']) }}
                      {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
