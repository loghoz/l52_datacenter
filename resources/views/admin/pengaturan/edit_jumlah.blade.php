@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jumlah Soal
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Jumlah Soal</h3>
                    </div>
                    <div class="box-body">
                          {!! Form::model($jumlah_soal, ['route' => ['admin.jumlah.update', $jumlah_soal],'method' =>'patch','class'=>'form-horizontal'])!!}
                              @include('form._admin_pengaturan_jum_soal', ['model' => $jumlah_soal])
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
