@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Pengaturan
        </h1>
    </section>

    <section class="content">
        <div class="row">

          <div class="col-md-12">
            {{-- agama --}}
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Agama</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.agama.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_agama')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Agama</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($agama as $item)
                                <tr>
                                  <td>{{ $item->agama }}</td>
                                  <td align="center"><a href="{{ route('admin.agama.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.agama.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $agama->links() }}
                        </div>

                    </div>
                </div>
            </div>

            {{-- jabatan --}}
            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jabatan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.jabatan.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_jabatan')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Jabatan</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($jabatan as $item)
                                <tr>
                                  <td>{{ $item->jabatan }}</td>
                                  <td align="center"><a href="{{ route('admin.jabatan.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.jabatan.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $jabatan->links() }}
                        </div>
                    </div>
                </div>
            </div>

            {{-- jam --}}
            <div class="col-md-4">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jam</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.jam.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_jam')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Jam</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($jam as $item)
                                <tr>
                                  <td align="center">{{ $item->jam }}</td>
                                  <td align="center"><a href="{{ route('admin.jam.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.jam.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $jam->links() }}
                        </div>

                    </div>
                </div>
            </div>
          </div>

          <div class="col-md-12">
            {{-- pekerjaan --}}
            <div class="col-md-4">
                <div class="box box-info">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pekerjaan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.pekerjaan.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_pekerjaan')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Pekerjaan</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($pekerjaan as $item)
                                <tr>
                                  <td>{{ $item->pekerjaan }}</td>
                                  <td align="center"><a href="{{ route('admin.pekerjaan.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.pekerjaan.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $pekerjaan->links() }}
                        </div>

                    </div>
                </div>
            </div>

            {{-- pendidikan --}}
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pendidikan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.pendidikan.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_pendidikan')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Pendidikan</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($pendidikan as $item)
                                <tr>
                                  <td>{{ $item->pendidikan }}</td>
                                  <td align="center"><a href="{{ route('admin.pendidikan.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.pendidikan.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $pendidikan->links() }}
                        </div>

                    </div>
                </div>
            </div>

            {{-- penghasilan --}}
            <div class="col-md-4">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Penghasilan</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.penghasilan.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_penghasilan')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Penghasilan</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($penghasilan as $item)
                                <tr>
                                  <td>{{ $item->penghasilan }}</td>
                                  <td align="center"><a href="{{ route('admin.penghasilan.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.penghasilan.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $penghasilan->links() }}
                        </div>

                    </div>
                </div>
            </div>
          </div>

          <div class="col-md-12">
            {{-- status --}}
            <div class="col-md-4">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Status</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.status.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_status')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Status</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($status as $item)
                                <tr>
                                  <td>{{ $item->status }}</td>
                                  <td align="center"><a href="{{ route('admin.status.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.status.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $status->links() }}
                        </div>

                    </div>
                </div>
            </div>

            {{-- tahun ajaran --}}
            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tahun Ajaran</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['url' => 'admin/ta', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_ta')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td colspan="3" align="center">Tahun Ajaran</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($ta as $item)
                                <tr>
                                  <td>{{ $item->tahun_ajaran }}</td>
                                  <td align="center"><a href="{{ route('tahun.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['tahun.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $ta->links() }}
                        </div>

                    </div>
                </div>
            </div>

            {{-- jumlah soal --}}
            {{--  <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jumlah Soal</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.pengaturan.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_pengaturan_jum_soal')
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <thead>
                              <tr>
                                <td align="center">Ujian</td>
                                <td align="center">Jumlah Soal</td>
                                <td colspan="2"></td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach($jumlah_soal as $item)
                                <tr>
                                  <td align="center">{{ $item->ujian }}</td>
                                  <td align="center">{{ $item->jumlah_soal }}</td>
                                  <td align="center"><a href="{{ route('admin.jumlah.edit', $item->id) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a></td>
                                  <td align="center">
                                    {{ Form::open(['route' => ['admin.jumlah.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                        {{ Form::button('<i class="fa fa-trash-o"></i>', ['type'=>'submit','class' => 'btn btn-danger js-submit-confirm']) }}
                                    {{ Form::close() }}
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                          {{ $jumlah_soal->links() }}
                        </div>

                    </div>
                </div>
            </div>  --}}

          </div>

        </div>
    </section>
@endsection
