@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Status
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Status</h3>
                    </div>
                    <div class="box-body">
                          {!! Form::model($status, ['route' => ['admin.status.update', $status],'method' =>'patch','class'=>'form-horizontal'])!!}
                              @include('form._admin_pengaturan_status', ['model' => $status])
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
