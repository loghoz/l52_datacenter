@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Agama
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Agama</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($agama, ['route' => ['admin.agama.update', $agama],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_pengaturan_agama', ['model' => $agama])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
