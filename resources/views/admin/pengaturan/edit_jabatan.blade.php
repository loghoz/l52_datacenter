@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jabatan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Jabatan</h3>
                    </div>
                    <div class="box-body">
                          {!! Form::model($jabatan, ['route' => ['admin.jabatan.update', $jabatan],'method' =>'patch','class'=>'form-horizontal'])!!}
                              @include('form._admin_pengaturan_jabatan', ['model' => $jabatan])
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
