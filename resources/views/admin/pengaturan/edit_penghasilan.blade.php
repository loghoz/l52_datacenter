@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Penghasilan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Penghasilan</h3>
                    </div>
                    <div class="box-body">
                          {!! Form::model($penghasilan, ['route' => ['admin.penghasilan.update', $penghasilan],'method' =>'patch','class'=>'form-horizontal'])!!}
                              @include('form._admin_pengaturan_penghasilan', ['model' => $penghasilan])
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
