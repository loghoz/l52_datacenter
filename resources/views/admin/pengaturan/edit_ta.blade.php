@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Tahun Ajaran
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-6">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Tahun Ajaran</h3>
                    </div>
                    <div class="box-body">
                          {!! Form::model($ta, ['route' => ['tahun.update', $ta],'method' =>'patch','class'=>'form-horizontal'])!!}
                              @include('form._admin_pengaturan_ta', ['model' => $ta])
                          {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
