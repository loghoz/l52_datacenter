@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Kelas</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-8">
                            {{--  {!! Form::open(['url' => 'admin/absensi', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <div class="form-group {!! $errors->has('cari_kelas') ? 'has-error' : '' !!}">
                                    {!! Form::select('cari_kelas',$lkelas ,null,array('class'=>'form-control')) !!}
                                    {!! $errors->first('cari_kelas', '<p class="help-block">:message</p>') !!}
                                </div>
                                {!! Form::submit('Pilih Kelas', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection