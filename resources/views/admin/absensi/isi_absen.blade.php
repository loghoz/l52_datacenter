@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Absen Kelas</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-3">
                            <h4>Guru<br>
                                Mata Pelajaran<br>
                                Kelas</h4>
                        </div>
                        <div class="col-md-3">
                            <h4>: {{ $jadwal->guru->nama }}<br>
                                : {{ $jadwal->matpel->mata_pelajaran }}<br>
                                : {{ $jadwal->kode_kelas }}<br>
                                </h4>
                        </div>
                        <div class="col-md-3">
                            <h4>Jam<br>
                                Hari<br>
                                Tanggal</h4>
                        </div>
                        <div class="col-md-3">
                            <h4>: {{ $jadwal->jam_mulai }} - {{ $jadwal->jam_selesai }}<br>
                                : {{ $hari->hari }}<br>
                                : {{ $tanggal }}</h4>
                        </div>
                        <hr>
                    </div>

                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.absensi.store', 'class'=>'form-horizontal'])!!}
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th align="center">NIS</th>
                                        <th align="center">Nama</th>
                                        <th>H</th>
                                        <th>A</th>
                                        <th>S</th>
                                        <th>I</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($absen as $item)
                                        <tr>
                                            <td>{{ $item->no_induk }}</td>
                                            <td>{{ $item->nama }}</td>
                                            <td>{{ Form::radio('absensi['.$item->no_induk.']', 'H' ) }}</td>
                                            <td>{{ Form::radio('absensi['.$item->no_induk.']', 'I' ) }}</td>
                                            <td>{{ Form::radio('absensi['.$item->no_induk.']', 'S' ) }}</td>
                                            <td>{{ Form::radio('absensi['.$item->no_induk.']', 'A' ) }}</td>
                                        </tr>
                                        <input type="hidden" name="guru_id[]" value="{{ $jadwal->guru->id }}"/>
                                        <input type="hidden" name="siswa_id[]" value="{{ $item->id }}"/>
                                        <input type="hidden" name="jadwal_id[]" value="{{ $jadwal->id }}"/>
                                        <input type="hidden" name="kelas_id[]" value="{{ $item->kelas_id }}"/>
                                        <input type="hidden" name="hari_id[]" value="{{ $hari->id }}"/>
                                        <input type="hidden" name="tanggal[]" value="{{ $tanggal }}"/>
                                        <input type="hidden" name="tahun_ajaran[]" value="{{ Cookie::get('tahun_ajaran') }}">
                                    @empty
                                        
                                    @endforelse
                                </tbody>
                            </table>
                            <div class="btn-group pull-right">
                                {!! Form::reset("Batal", ['class' => 'btn btn-default']) !!}
                                {!! Form::submit("Simpan", ['class' => 'btn btn-primary']) !!}
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection