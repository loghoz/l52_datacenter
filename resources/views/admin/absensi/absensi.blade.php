@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Daftar Kelas</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-8">
                            {!! Form::open(['url' => 'admin/absensi', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <div class="form-group {!! $errors->has('cari_kelas') ? 'has-error' : '' !!}">
                                    {!! Form::select('cari_kelas',$lkelas ,null,array('class'=>'form-control')) !!}
                                    {!! $errors->first('cari_kelas', '<p class="help-block">:message</p>') !!}
                                </div>
                                {!! Form::submit('Pilih Kelas', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4">
                            <h4>Hari ini : {{ $hari->hari }}, {{ $tanggal }}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Jadwal</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td align="center">Waktu</td>
                                        <td align="center">Guru</td>
                                        <td align="center">Mata Pelajaran</td>
                                        <td align="center">Absensi</td>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    @forelse ($jadwal as $item)
                                        <tr>
                                            <td align="center">{{ $item->jam_mulai }} - {{ $item->jam_selesai }}</td>
                                            <td>{{ $item->guru->nama }}</td>
                                            <td align="center">{{ $item->matpel->mata_pelajaran }}</td>
                                            <td align="center">
                                                <a href="{{ url('/admin/absensi/absensi', $item->id) }}"><span class="fa fa-edit"></span></a>
                                            </td>
                                        </tr>
                                    @empty
                                        
                                    @endforelse
                                        
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-danger">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Absensi Siswa</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-12">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <td rowspan="2" align="center">Nama</td>
                                        <td colspan="8" align="center">Jam</td>
                                    </tr>
                                    <tr>
                                        <td align="center">1</td>
                                        <td align="center">2</td>
                                        <td align="center">3</td>
                                        <td align="center">4</td>
                                        <td align="center">5</td>
                                        <td align="center">6</td> 
                                        <td align="center">7</td>
                                        <td align="center">8</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if (empty($absensi))
                                        
                                    @else
                                        @forelse ($absensi as $item)
                                            <tr>
                                                <td align="center">{{ $item->nama }}</td>
                                                @forelse ($item->absensi as $absen)
                                                    @if ($absen->tanggal == $tanggal) 
                                                        <td align="center">
                                                            <a href="{{ route('admin.absensi.edit', $absen->id)}}">{{ $absen->absensi }}</a>
                                                        </td>
                                                    @endif
                                                @empty
                                                    
                                                @endforelse
                                            </tr>
                                        @empty
                                            
                                        @endforelse
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection