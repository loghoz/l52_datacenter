@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Absensi
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Absensi</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($absen, ['route' => ['admin.absensi.update', $absen],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_absen', ['model' => $absen])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
