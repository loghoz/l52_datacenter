<div class="box box-widget widget-user">
    <div class="widget-user-header bg-aqua-active">
        <h3 class="widget-user-username">{{ $gurus->nama }}</h3>
        <h5 class="widget-user-desc">{{ $gurus->jabatan->jabatan }}</h5>
    </div>
    <div class="widget-user-image">
        <img class="img-circle" src="{{ url('/img/guru/' . $gurus->guru_meta->photo) }}" alt="User Avatar">
    </div>
    <div class="box-footer">
        <div class="row">

            <div class="col-sm-6 border-right">
                <div class="description-block">
                    <button class="btn btn-primary" data-toggle="modal" data-target="#detail-{{ $gurus->id }}">Detail</button>

                    <div class="modal modal-primary fade" id="detail-{{ $gurus->id }}" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title" id="{{ $gurus->id }}">
                                        <img class="img-circle" src="{{ url('img/guru/' . $gurus->guru_meta->photo) }}" width="150" height="150" class="img-circle" alt="User Image"><br>
                                        {{ $gurus->nama }}<br>
                                        {{ $gurus->jabatan->jabatan }}
                                    </h4>
                                </div>

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <strong>Agama<br>
                                            No Induk<br>
                                            Tempat lahir<br>
                                            Status</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $gurus->agama->agama }}<br>
                                            {{ $gurus->no_induk }}<br>
                                            {{ $gurus->guru_meta->tempat_lahir }}<br>
                                            {{ $gurus->status->status }}
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Jenis Kelamin<br>
                                            NUPTK<br>
                                            Tanggal Lahir<br>
                                            Ijazah Terakhir</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $gurus->guru_meta->jk }}<br>
                                            {{ $gurus->guru_meta->nuptk }}<br>
                                            {{ $gurus->guru_meta->tanggal_lahir }}<br>
                                            {{ $gurus->ijazah->ijazah}} {{ $gurus->guru_meta->ijazah_jurusan}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br>Alamat : {{ $gurus->guru_meta->alamat }}, RT / RW : {{ $gurus->guru_meta->rt }} / {{ $gurus->guru_meta->rw }}<br><br>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <strong>Dusun / Desa<br>
                                            Kecamatan</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $gurus->guru_meta->desa }}<br>
                                            {{ $gurus->guru_meta->kecamatan }}
                                        </div>
                                        <div class="col-sm-3">
                                            <strong>Kabupaten / Kota<br>
                                            Provinsi</strong>
                                        </div>
                                        <div class="col-sm-3">
                                            {{ $gurus->guru_meta->kabupaten }}<br>
                                            {{ $gurus->guru_meta->provinsi }}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <br>Kode POS : {{ $gurus->guru_meta->kode_pos }} <=> Telp : {{ $gurus->guru_meta->telp }}
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button onclick="window.location.href='{{ route('admin.guru.edit', $gurus->id) }}'" class="btn btn-default">Ubah</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-sm-6">
                <div class="description-block">
                    {{ Form::open(['url' => ['admin/guru/hapus' ,$gurus->id] ,'method' => 'PUT']) }}
                      {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
