@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Tambah Karyawan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/guru') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data Karyawan</a>
                <br><br>
            </div>

            <div class="col-md-12">
                {!! Form::open(['route' => 'admin.guru.store', 'class'=>'form-horizontal','files'=>true])!!}
                    @include('form._admin_guru')
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
