@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Guru Piket
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Guru Piket</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($gupit, ['route' => ['admin.gurupiket.update', $gupit],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_gurupiket', ['model' => $gupit])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
