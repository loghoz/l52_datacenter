@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Karyawan
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Karyawan</h3>
                    </div>
                    <div class="box-body">
                      <div class="col-md-4">
                        {!! Form::open(['url' => 'admin/guru', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Cari Karyawan....']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                          {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                        </div>
                        <div class="col-md-3">
                            {!! Form::open(['url' => 'admin/guru', 'method'=>'get', 'class'=>'form-inline'])!!}
                                <input type="hidden" name="q" value="">
                                {!! Form::submit('Semua Data Karyawan', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                      <div class="col-md-5" align="right">
                        <a href="{{ route('admin.guru.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Karwayan</a>
                        <a href="{{ route('admin.walikelas.index') }}" class="btn btn-success"><i class="fa fa-eye"></i> Wali Kelas</a>
                        <a href="{{ route('admin.gurupiket.index') }}" class="btn btn-info"><i class="fa fa-eye"></i> Guru Piket</a>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @forelse ($guru as $gurus)
                <div class="col-md-4">
                    @include('admin.guru._profil', ['guru' => $gurus])
                </div>
              @empty
              <div class="col-md-12 text-center">
                @if (isset($q))
                  <h1>:(</h1>
                  <p>Karyawan tidak ditemukan.</p>
                @endif
                  <p><a href="{{ url('/admin/guru') }}">Lihat semua data Karyawan<i class="fa fa-arrow-right"></i></a></p>
              </div>
            @endforelse
        </div>
        {{ $guru->appends(compact('q', 'page'))->links() }}
    </section>
@endsection
