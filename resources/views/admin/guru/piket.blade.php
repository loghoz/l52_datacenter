@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Guru Piket
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/guru') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Data Karyawan</a>
                <br><br>
            </div>

            <div class="col-md-12">

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Guru Piket</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.gurupiket.store', 'class'=>'form-horizontal'])!!}
                            @include('form._admin_gurupiket')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Guru Piket</h3>
                    </div>
                    <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Hari</td>
                                <td>Guru</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($gupit as $gurupiket)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $gurupiket->hari->hari }}</td>
                                    <td>{{ $gurupiket->guru->nama }}</td>
                                    <td>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <a href="{{ route('admin.gurupiket.edit', $gurupiket->id) }}" class="btn btn-primary">Ubah</a>
                                            </div>
                                            <div class="col-md-6">
                                                {!! Form::model($gurupiket, ['route' => ['admin.gurupiket.destroy', $gurupiket], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                                  {!! Form::submit('Hapus', ['class'=>'btn btn-danger js-submit-confirm']) !!}
                                                {!! Form::close()!!}
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $gupit->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
