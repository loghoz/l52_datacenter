@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Orang Tua / Wali
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                {!! Form::model($siswawali, ['route' => ['admin.siswawali.update', $siswawali],'method' =>'patch','class'=>'form-horizontal'])!!}
                    @include('form._admin_edit_siswawali', ['model' => $siswawali])
                {!! Form::close() !!}
            </div>
        </div>
    </section>
@endsection
