@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Kelas
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Kelas</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.kelas.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_kelas')
                        {!! Form::close() !!}
                    </div>
                </div>

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Data Kelas</h3>
                    </div>
                    <div class="box-body">
                        <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/kelas', 'method'=>'get', 'class'=>'form-inline'])!!}
                            <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                                {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Cari Kelas....']) !!}
                                {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                            </div>
                            {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4">
                            {!! Form::open(['url' => 'admin/kelas', 'method'=>'get', 'class'=>'form-inline'])!!}
                            <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                                <input type="hidden" name="q" value="">
                                {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                            </div>
                            {!! Form::submit('Semua Kelas', ['class'=>'btn btn-primary']) !!}
                            {!! Form::close() !!}
                        </div>
                        <div class="col-md-4">
                        </div>

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td>No</td>
                                <td>Kode Kelas</td>
                                <td>Tingkat</td>
                                <td>Jurusan</td>
                                <td>Aktif</td>
                                <td colspan="2" align="center">Action</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($kelas as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->kode_kelas }}</td>
                                    <td>{{ $item->tingkat }}</td>
                                    <td>{{ $item->jurusan->jurusan}}</td>
                                    <td>{{ $item->aktif }}</td>
                                    <td>
                                      <div class="col-md-12">
                                          <div class="col-md-6">
                                              <a href="{{ route('admin.kelas.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                          </div>
                                          <div class="col-md-6">
                                              {{ Form::open(['route' => ['admin.kelas.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                                  {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                              {{ Form::close() }}
                                          </div>
                                      </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $kelas->appends(compact('q','page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
