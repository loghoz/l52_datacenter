<div class="user-panel">
  <div class="pull-left image">
    <img src="{{ url('img/avatar4.png')}}" class="img-circle" alt="User Image">
  </div>
  <div class="pull-left info">
    <p>Admin</p>
		<a href="{{ url('/tahun') }}">T.A. {{ Cookie::get('tahun_ajaran') }}</a> 
  </div>
</div>

<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MENU</li>

	{{--  BERANDA  --}}
	<li>
		<a href="{{ url('/admin/beranda') }}">
			<i class="fa fa-home"></i>
			<span>Beranda</span>
		</a>
	</li>

	{{--  DATA SEKOLAH  --}}
	<li class="treeview">
		<a href="#">
			<i class="fa fa-file"></i>
			<span>Data Sekolah</span>
			<span class="pull-right-container">
				<i class="fa fa-angle-left pull-right"></i>
			</span>
		</a>
		<ul class="treeview-menu">
			<li><a href="{{ url('/admin/siswa') }}"><i class="fa fa-user"></i>Siswa</a></li>
			<li><a href="{{ url('/admin/guru') }}"><i class="fa fa-user"></i>Karyawan</a></li>
			<li><a href="{{ url('/admin/jurusan') }}"><i class="fa fa-file"></i> <span>Jurusan</span></a></li>
			<li><a href="{{ url('/admin/kelas') }}"><i class="fa fa-file"></i> <span>Kelas</span></a></li>
			<li><a href="{{ url('/admin/mapel/na') }}"><i class="fa fa-file"></i> <span>Mata Pelajaran</span></a></li>
			<li><a href="{{ url('/admin/jadwal/jadwal') }}"><i class="fa fa-calendar"></i> <span>Jadwal Pelajaran</span></a></li>
			<li><a href="{{ url('/admin/akun/admin') }}"><i class="fa fa-key"></i> <span>Akun</span></a></li>
			<li><a href="{{ url('/admin/sampah') }}"><i class="fa fa-trash-o"></i> <span>Kotak Sampah</span></a></li>
		</ul>
	</li>

	{{--  ABSENSI  --}}
	{{--  DATA SEKOLAH  --}}
	<li class="treeview">
			<a href="#">
				<i class="fa fa-calendar"></i>
				<span>Absensi</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="{{ url('/admin/absensi') }}"><i class="fa fa-edit"></i>Absensi</a></li>
				<li><a href="{{ url('/admin/absensi/data') }}"><i class="fa fa-file"></i>Data Absensi</a></li>
			</ul>
	</li>

	{{--  pengaturan  --}}
	<li>
		<a href="{{ url('/admin/pengaturan') }}">
			<i class="fa fa-cog"></i>
			<span>Pengaturan</span>
		</a>
	</li>

</ul>