@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jadwal Mengajar Guru
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-8">
              {!! Form::open(['url' => 'admin/jadwal/guru ', 'method'=>'get', 'class'=>'form-inline'])!!}

                <div class="form-group {!! $errors->has('guru') ? 'has-error' : '' !!}">
                    {!! Form::select('guru',$guru ,null,['class'=>'form-control']) !!}
                    {!! $errors->first('guru', '<p class="help-block">:message</p>') !!}
                </div>

                {!! Form::submit('Lihat', ['class'=>'btn btn-primary']) !!}
              {!! Form::close() !!}
            </div>
            <div class="col-md-4" align="right">
              <a href="{{ url('/admin/jadwal/jadwal') }}" class="btn btn-primary"><span class="fa fa-plus"></span> Tambah Jadwal</a>
            </div>
            <div class="col-md-12">
              <br>
                <center>
                    @foreach ($hasila as $guru)
                        <div class="widget-user-image">
                            <img class="img-circle" src="{{ url('img/guru/' . $guru->guru_meta->photo) }}" width="150px" height="150px" alt="User Avatar">
                        </div>
                        <font size="5">
                            {{ $guru->nama }}<br>
                        </font>
                    @endforeach
                </center>
                <div class="box box-danger">
                    <div class="box-body">

                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td>Hari</td>
                                  <td>Waktu</td>
                                  <td>Mata Pelajaran</td>
                                  <td>Jumlah Jam</td>
                                  <td>Kelas</td>
                                  <td></td>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($hasil as $item)
                                  <tr>
                                      <td>
                                          @if ($item->hari->hari == "Senin")
                                              <span class="label label-primary">S</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Selasa")
                                              <span class="label label-success">S</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Rabu")
                                              <span class="label label-warning">R</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Kamis")
                                              <span class="label label-info">K</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Jumat")
                                              <span class="label label-danger">J</span> {{ $item->hari->hari }}
                                          @elseif($item->hari->hari == "Sabtu")
                                              <span class="label label-primary">S</span> {{ $item->hari->hari }}
                                          @endif

                                      </td>
                                      <td>{{ $item->jam_mulai }} - {{ $item->jam_selesai }}</td>
                                      <td>{{ $item->matpel->mata_pelajaran }}</td>
                                      <td>{{ $item->jam_ajar }} Jam</td>
                                      <td>{{ $item->kode_kelas }}</td>
                                      <td align="center">
                                            {{ Form::open(['url' => ['admin/jadwal/guru/hapus' , $item->id] ,'method' => 'PUT']) }}
                                              {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                            {{ Form::close() }}
                                        </td>
                                  </tr>
                                @endforeach
                            </tbody>
                            <tbody>
                              <tr>
                                  <td align="center" colspan="3">Total Jam Mengajar</td>
                                  <td>{{ $jam }} Jam</td>
                                  <td></td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
