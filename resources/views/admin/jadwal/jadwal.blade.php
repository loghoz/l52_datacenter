@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Jadwal Pelajaran
        </h1>
    </section>

    <section class="content">
        <div class="row">
            {{--  MENU  --}}
            <div class="col-md-12" align="right">
                <a href="{{ url('/admin/jadwal/guru') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Jadwal Guru</a>
                <br><br>
            </div>

            <div class="col-md-12">
                {{--  TAMBAH JADWAL GURU  --}}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Jadwal</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::open(['route' => 'admin.jadwal.jadwal.store', 'class'=>'form-horizontal'])!!}
                            @include('form._admin_jadwal')
                        {!! Form::close() !!}
                    </div>
                </div>

                {{--  LIHAT JADWAL KELAS  --}}
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lihat Jadwal Pelajaran</h3>
                    </div>

                    <div class="box-body">                       
                        {!! Form::open(['url' => 'admin/jadwal/jadwal', 'method'=>'get', 'class'=>'form-inline'])!!}

                            <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                                {!! Form::select('q',$hari ,null,['class'=>'form-control']) !!}
                                {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('r') ? 'has-error' : '' !!}">
                                {!! Form::select('r',$kelas ,null,['class'=>'form-control']) !!}
                                {!! $errors->first('r', '<p class="help-block">:message</p>') !!}
                            </div>

                            {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>

                    <div class="box-body">
                        <center>
                            <font size="6">
                              @foreach ($h_kelas as $element1)
                                {{ $element1->kode_kelas }}
                              @endforeach
                            </font><br>
                            <font size="5">
                              @foreach ($h_hari as $element)
                                {{ $element->hari }}
                              @endforeach
                            </font><br>
                        </center>

                        <table class="table table-bordered">
                            <thead>
                              <tr>
                                  <td align="center">Waktu</td>
                                  <td align="center">Guru</td>
                                  <td align="center">Mata Pelajaran</td>
                                  <td align="center">Aksi</td>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($jadwal as $item)
                                  <tr>
                                      <td align="center">{{ $item->jam_mulai }} - {{ $item->jam_selesai }}</td>
                                      <td>{{ $item->guru->nama }}</td>
                                      <td align="center">{{ $item->matpel->mata_pelajaran }}</td>
                                      {{-- <td align="center">
                                          <button onclick="window.location.href='{{ route('admin.jadwal.jadwal.edit', $item->id) }}'" class="btn btn-primary">Ubah</button>
                                      </td> --}}
                                      <td align="center">
                                          {{ Form::open(['route' => ['admin.jadwal.jadwal.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                            {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                          {{ Form::close() }}
                                      </td>
                                  </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
