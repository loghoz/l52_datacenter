@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Akun
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
              <a href="{{ url('/admin/akun/siswa') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Akun Siswa</a>
              <br><br>
            </div>
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Ubah Akun</h3>
                    </div>
                    <div class="box-body">
                        {!! Form::model($akun, ['route' => ['admin.akun.siswa.update', $akun],'method' =>'patch','class'=>'form-horizontal'])!!}
                            @include('form._admin_akun', ['model' => $akun])
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
