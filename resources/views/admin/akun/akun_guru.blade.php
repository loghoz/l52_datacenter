@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Akun
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
              <a href="{{ url('/admin/akun/admin') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Akun Admin</a>
              <a href="{{ url('/admin/akun/siswa') }}" class="btn btn-success"><span class="fa fa-eye"></span> Akun Siswa</a>
              <br><br>
            </div>

            <div class="col-md-12">

                <div class="box box-primary">

                    <div class="box-header with-border">
                        <h3 class="box-title">Akun Karyawan</h3>
                    </div>

                    <div class="box-body">
                        {!! Form::open(['url' => 'admin/akun/guru', 'method'=>'get', 'class'=>'form-inline'])!!}
                          <div class="form-group {!! $errors->has('q') ? 'has-error' : '' !!}">
                              {!! Form::text('q', isset($q) ? $q : null, ['class'=>'form-control', 'placeholder' => 'Cari Akun Karyawan ...']) !!}
                              {!! $errors->first('q', '<p class="help-block">:message</p>') !!}
                          </div>
                        {!! Form::submit('Cari', ['class'=>'btn btn-primary']) !!}
                        {!! Form::close() !!}
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td align="center">No</td>
                                <td align="center">Username</td>
                                <td align="center">Password</td>
                                <td align="center">Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($akun as $item)
                                <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td align="center">*****</td>
                                    <td align="center">
                                        <a href="{{ route('admin.akun.guru.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $akun->appends(compact('q','page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
