@extends('layouts.app')

@section('content')

    <section class="content-header">
        <h1>
            Akun
        </h1>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-md-12" align="right">
              <a href="{{ url('/admin/akun/guru') }}" class="btn btn-primary"><span class="fa fa-eye"></span> Akun Karyawan</a>
              <a href="{{ url('/admin/akun/siswa') }}" class="btn btn-success"><span class="fa fa-eye"></span> Akun Siswa</a>
              <br><br>
            </div>
            <div class="col-md-12">

                <div class="box box-primary">

                    <div class="box-header with-border">
                        <h3 class="box-title">Tambah Akun Admin</h3>
                    </div>

                    <div class="box-body">
                      {!! Form::open(['route' => 'admin.akun.admin.store', 'class'=>'form-horizontal'])!!}
                          @include('form._admin_akun_tambah')
                      {!! Form::close() !!}
                    </div>
                </div>
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Akun Admin</h3>
                    </div>

                    <div class="box-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <td align="center">No</td>
                                <td align="center">Username</td>
                                <td align="center">Password</td>
                                <td colspan="2" align="center">Aksi</td>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($akun as $item)
                                <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td>{{ $item->username }}</td>
                                    <td align="center">*****</td>
                                    <td align="center">
                                        <a href="{{ route('admin.akun.admin.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                    </td>
                                    <td align="center">
                                        {{ Form::open(['route' => ['admin.akun.admin.destroy' , $item->id] ,'method' => 'DELETE']) }}
                                            {{ Form::submit('Hapus', ['class' => 'btn btn-danger js-submit-confirm']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $akun->appends(compact('page'))->links() }}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
