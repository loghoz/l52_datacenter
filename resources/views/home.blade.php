@extends('layouts.app')

@section('content')
    <div align="center" style="padding-top:10px;">
        <img src="{{asset('img/logo.png')}}" width="200px" height="200px" alt="logo pelita" /><br>
        <h1>SMK PELITA GEDONGTATAAN<br>
        KABUPATEN PESAWARAN - LAMPUNG</h1>
    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Selamat Datang</h3>
                    </div>
                    <div class="box-body">
                        <p>
                          Selamat datang di website DATA Center SMK Pelita Gedongtataan
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
