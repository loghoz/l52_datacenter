<table>
    <thead>
    <tr>
        <th>Kode Kelas</th>
        <th>Jurusan</th>
    </tr>
    </thead>
    <tbody>
    @foreach($kelas as $item)
        <tr>
            <td>{{ $item->kode_kelas }}</td>
            <td>{{ $item->jurusan->jurusan }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
