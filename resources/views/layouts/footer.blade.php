<div class="pull-right hidden-xs">
    <b>Version</b> 1.0
</div>
<strong>Copyright &copy; 2016 <a data-toggle="modal" data-target="#detail">Erwin Dianto</a></strong>

<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div align="center">
                  <h4 class="modal-title" id="header">
                      <img class="img-circle" src="{{ url('/img/admin-1.jpg') }}" width="150" height="150" class="img-circle" alt="User Image"><br>
                      <br>ERWIN DIANTO
                  </h4>
                </div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class='table-responsive'>
                          <table class='table table-striped table-bordered table-hover table-condensed'>
                            <tbody>
                              <tr>
                                <td>Nama</td>
                                <td>Erwin Dianto</td>
                              </tr>
                              <tr>
                                <td>TTL</td>
                                <td>Krandegan, 08 Januari 1992</td>
                              </tr>
                              <tr>
                                <td>Agama</td>
                                <td>100% Islam</td>
                              </tr>
                              <tr>
                                <td>Alamat</td>
                                <td>Jl. Raya Agmad Yani, Gadingrejo Timur<br>Kecamatan Gadingrejo<br>Kabupaten Pringsewu - Lampung</td>
                              </tr>
                              <tr>
                                <td>Telp</td>
                                <td>0896-3107-3926</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12" align="center">
                      <a target="_blank" href="https://www.facebook.com/erwin.dianto8"><i class="fa fa-facebook"></i><a>
                      <a target="_blank" href="https://www.instagram.com/erwindianto/"><i class="fa fa-instagram"></i><a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
            </div>
        </div>
    </div>
</div>
