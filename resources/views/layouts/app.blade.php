<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SMK PELITA</title>

    <!-- Styles -->
    <link href="{{ asset('css/adminlte.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

    {{-- Header --}}
    <header class="main-header">
        @include('layouts.header')
    </header>

    {{-- Menu --}}
    <aside class="main-sidebar">
        <section class="sidebar">
            @if(Auth::user()->role === 'admin')
                @include('admin.menu')
            @elseif(Auth::user()->role === 'guru')
                @include('guru.menu')
            @else
                @include('siswa.menu')
            @endif
        </section>
    </aside>

    {{-- Content --}}
    <div class="content-wrapper">
        @yield('content')
    </div>
    
    {{-- Footer --}}
    <footer class="main-footer">
        @include('layouts.footer')
    </footer>
    
    <!-- Scripts -->
    <script src="{{ asset('js/adminlte.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
</body>
</html>