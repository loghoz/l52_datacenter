<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset ('gambar/favicon.ico') }}">

    <title>SMK Pelita</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <link rel="stylesheet" href="{{ asset ('adminlte/bootstrap/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset ('adminlte/dist/css/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('adminlte/dist/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset ('adminlte/dist/css/waktu.css') }}">
    <link href="{{ asset ('sweetalert/dist/sweetalert.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset ('adminlte/dist/css/toastr.css') }}">

    <style>
        body {
            font-family: 'Lato';
            background-color: #f8f8f8 ;
        }
    </style>
</head>
<body class="body" onload=init() onunload=keluar()>

    <div class="col-md-12">
        @if(Auth::check())

        {{-- Content --}}
        @yield('content')

        @endif
    </div>

    <!-- JavaScripts -->
    <script src="{{ asset ('adminlte/plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
    <script src="{{ asset ('adminlte/js/pemapel.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jquery.countdown.js') }}"></script>
    <script src="{{ asset ('adminlte/js/jawaban.js') }}"></script>
    <script src="{{ asset ('adminlte/js/toastr.js') }}"></script>
    <script src="{{ asset ('adminlte/js/toastr.min.js') }}"></script>

    <script>
        @if(Session::has('success'))
        		toastr.success("{{ Session::get('success') }}");
        @endif

        @if(Session::has('info'))
        		toastr.info("{{ Session::get('info') }}");
        @endif

        @if(Session::has('warning'))
        		toastr.warning("{{ Session::get('warning') }}");
        @endif

        @if(Session::has('error'))
        		toastr.error("{{ Session::get('error') }}");
        @endif
    </script>
    <script type="text/javascript" src="{{ asset ('sweetalert/dist/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset ('sweetalert/dist/test.js') }}"></script>
    <script src="{{ asset ('adminlte/dist/js/jquery-ui.min.js') }}"></script>

    <script src="{{ asset ('adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
</body>
</html>
