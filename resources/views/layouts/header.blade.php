@if(Auth::user()->role === 'admin')
  <a href="{{ url('admin/beranda') }}" class="logo">
@elseif(Auth::user()->role === 'guru')
  <a href="{{ url('guru/beranda') }}" class="logo">
@else
  <a href="{{ url('siswa/beranda') }}" class="logo">
@endif
  <span class="logo-mini"><b>D</b>C</span>
  <span class="logo-lg"><b>DATA</b>Center</span>
</a>

<nav class="navbar navbar-static-top">
  <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
    <span class="sr-only">Toggle navigation</span>
  </a>

  {{--  ADMIN  --}}
  @if(Auth::user()->role === 'admin')
  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav">

      <li class="dropdown user user-menu">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <img src="{{ asset('img/avatar4.png') }}" class="user-image" alt="User Image">
          <span class="hidden-xs">
            Admin
          </span>
        </a>
        <ul class="dropdown-menu">

          <li class="user-header">
            <img src="{{ asset('img/avatar4.png') }}" class="img-circle" alt="User Image">
            <p>Admin</p>
          </li>
  
  {{--  GURU  --}}
  @elseif(Auth::user()->role === 'guru')
  <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
  
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="{{ asset('img/guru') }}/{{ Cookie::get('photo') }}" class="user-image" alt="User Image">
            <span class="hidden-xs">{{ Cookie::get('nama') }}</span>
          </a>
          <ul class="dropdown-menu">
            <li class="user-header">
                <img src="{{ asset('img/guru') }}/{{ Cookie::get('photo') }}" class="img-circle" alt="User Image">
                <p>{{ Cookie::get('nama') }}</p>
            </li>

  @endif
          <!-- Menu Footer-->
          <li class="user-footer">
            <div class="pull-left">
            </div>
            <div class="pull-right">
              <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Keluar</a>
            </div>
          </li>
        </ul>
      </li>
    </ul>
  </div>
</nav>