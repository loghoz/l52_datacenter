var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
var dirs = {
    'vendor': "./resources/assets/adminlte"
}

elixir(function(mix) {
    // Compile AdminLTE scripts to single file.
    mix.scripts([
        dirs.vendor + '/bower_components/jquery/dist/jquery.min.js',
        dirs.vendor + '/bower_components/jquery-ui/jquery-ui.min.js',
        dirs.vendor + '/bower_components/bootstrap/dist/js/bootstrap.min.js',
        dirs.vendor + '/bower_components/raphael/raphael.min.js',
        dirs.vendor + '/bower_components/morris.js/morris.min.js',
        dirs.vendor + '/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js',
        dirs.vendor + '/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js',
        dirs.vendor + '/plugins/jvectormap/jquery-jvectormap-world-mill-en.js',
        dirs.vendor + '/bower_components/jquery-knob/dist/jquery.knob.min.js',
        dirs.vendor + '/bower_components/moment/min/moment.min.js',
        dirs.vendor + '/bower_components/bootstrap-daterangepicker/daterangepicker.js',
        dirs.vendor + '/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
        dirs.vendor + '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js',
        dirs.vendor + '/bower_components/jquery-slimscroll/jquery.slimscroll.min.js',
        dirs.vendor + '/bower_components/fastclick/lib/fastclick.js',
        dirs.vendor + '/dist/js/adminlte.min.js',
        dirs.vendor + '/dist/js/pages/dashboard.js',
        dirs.vendor + '/dist/js/demo.js',
        dirs.vendor + '/dist/js/pemapel.js',
        // 'node_modules/sweetalert/dist/sweetalert.min.js',
    ], 'public/js/adminlte.js');

    // Compile AdminLTE css to single file.
    mix.styles([
        dirs.vendor + '/bower_components/bootstrap/dist/css/bootstrap.min.css',
        dirs.vendor + '/bower_components/font-awesome/css/font-awesome.min.css',
        dirs.vendor + '/bower_components/Ionicons/css/ionicons.min.css',
        dirs.vendor + '/dist/css/AdminLTE.min.css',
        dirs.vendor + '/dist/css/skins/_all-skins.min.css',
        dirs.vendor + '/bower_components/morris.js/morris.css',
        dirs.vendor + '/bower_components/jvectormap/jquery-jvectormap.css',
        dirs.vendor + '/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
        dirs.vendor + '/bower_components/bootstrap-daterangepicker/daterangepicker.css',
        dirs.vendor + '/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css'
    ], 'public/css/adminlte.css');

    // Copy AdminLTE assets.
    mix.copy(dirs.vendor + '/bower_components/font-awesome/fonts', 'public/fonts')
        .copy(dirs.vendor + '/bower_components/Ionicons/fonts', 'public/fonts')
        .copy(dirs.vendor + '/bower_components/bootstrap/dist/fonts', 'public/fonts')
        .copy(dirs.vendor + '/dist/img', 'public/img');

    mix.copy('resources/assets/img', 'public/img');
    mix.copy(dirs.vendor + '/dist/img/avatar1.png', 'public/img/siswa');
    mix.copy(dirs.vendor + '/dist/img/avatar3.png', 'public/img/siswa');
    mix.copy(dirs.vendor + '/dist/img/avatar2.png', 'public/img/guru');
    mix.copy(dirs.vendor + '/dist/img/avatar5.png', 'public/img/guru');
    // mix.copy(dirs.vendor + '/dist/js/pemapel.js', 'public/js');
});
