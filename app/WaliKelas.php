<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WaliKelas extends Model
{
    protected $table = 'wali_kelas';
    
    protected $fillable = [
        'guru_id','kelas_id','tahun_ajaran',
    ];

    //relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }

    //relasi ke kelas
    public function kelas()
    {
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }
}
