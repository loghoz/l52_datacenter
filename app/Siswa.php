<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = 'siswas';
    protected $with = ['siswa_meta','siswa_wali','kelas','agama','absensi'];
    protected $fillable = ['no_induk','nama','agama_id','kelas_id','tahun_ajaran','hapus'];

    //relasi one to one ke siswa meta
    public function siswa_meta()
    {
        return $this->belongsTo('App\SiswaMeta','no_induk');
    }
    
    //relasi one to one ke siswa wali
    public function siswa_wali()
    {
        return $this->belongsTo('App\SiswaWali','no_induk');
    }

    //relasi ke agama
    public function agama()
    {
        return $this->belongsTo('App\Agama', 'agama_id');
    }

    //relasi ke kelas
    public function kelas()
    {
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }

    //relasi one to many ke absensi
    public function absensi()
    {
        return $this->hasMany('App\Absensi');
    }
}
