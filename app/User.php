<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $fillable = [
        'no_induk','username', 'email', 'password','api_token','role'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
}
