<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MataPelajaran extends Model
{
    protected $table = 'mata_pelajarans';
    protected $with = ['jurusan'];
    protected $fillable = [
        'jurusan_id','kode_mat_pel','jenis_mat_pel','mata_pelajaran','deskripsi','tahun_ajaran',
    ];

    // relasi ke jurusan
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }

    //relasi one to many ke jadwal
    public function jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }

    //relasi one to many ke pelapel
    public function pemapel()
    {
        return $this->hasMany('App\Pemapel');
    }
}
