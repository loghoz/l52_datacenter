<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Status;

class StatusController extends Controller
{
    public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('role:admin');
  }

  public function index()
  {
      //
  }

  public function create()
  {
      //
  }

  public function store(Request $request)
  {
    Status::create($request->all());
    //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
    return redirect()->route('admin.pengaturan.index');
  }

  public function show($id)
  {
      //
  }

  public function edit($id)
  {
      $status = Status::findOrFail($id);

      return view('admin.pengaturan.edit_status', compact('status'));
  }

  public function update(Request $request, $id)
  {
      $status = Status::findOrFail($id);
      $data = $request->only('status');
      $status->update($data);
    //   Session::flash('info', 'Status telah dirubah.');
      return redirect()->route('admin.pengaturan.index');
  }

  public function destroy($id)
  {
    Status::find($id)->delete();
    // Session::flash('error', 'Status telah dihapus.');

    return redirect()->route('admin.pengaturan.index');
  }
}
