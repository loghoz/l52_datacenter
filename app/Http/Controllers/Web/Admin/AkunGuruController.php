<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\AkunEditRequest;
use App\Http\Controllers\Controller;

use App\User;

class AkunGuruController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $akun = User::where('role','guru')
            ->where('username', 'LIKE', '%'.$q.'%')
            ->orderBy('username','asc')
            ->paginate(10);

        return view('admin.akun.akun_guru', compact('akun', 'no','q'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
      $akun = User::select('username','id')->findOrFail($id);

      return view('admin.akun.edit_guru', compact('akun'));
    }

    public function update(Request $request, $id)
    {
        $akun = User::findOrFail($id);

        $password = $request->get('password');

        // $data = $request->only('username');

        if ($password == '') {
        } else {
          $data['password'] = bcrypt($request->get('password'));
        }

        $akun->update($data);

        // Session::flash('info', 'Akun '.$request->get('username').' telah dirubah.');
        return redirect()->route('admin.akun.guru.index');
    }

    public function destroy($id)
    {
        //
    }
}
