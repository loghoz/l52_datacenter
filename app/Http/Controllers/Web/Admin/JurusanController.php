<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\JurusanRequest;
use App\Http\Controllers\Controller;

use App\Jurusan;

class JurusanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $jurusan = Jurusan::where('kode_jurusan','not like','-')->paginate(5);
        return view('admin.jurusan.jurusan', compact('jurusan','no'));
    }

    public function create()
    {
        //
    }

    public function store(JurusanRequest $request)
    {
      Jurusan::create($request->all());
    //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
      return redirect()->route('admin.jurusan.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
      $jurusan = Jurusan::findOrFail($id);
      return view('admin.jurusan.edit_jurusan', compact('jurusan'));
    }

    public function update(JurusanRequest $request, $id)
    {
      $jurusan = Jurusan::findOrFail($id);

      $jurusan->update($request->all());
    //   Session::flash('info', 'Jurusan '.$request->get('jurusan').' telah dirubah.');
      return redirect()->route('admin.jurusan.index');
    }

    public function destroy($id)
    {
      $hapus = Jurusan::findOrFail($id);
    //   Session::flash('error', 'Jurusan '.$hapus->jurusan.' telah dihapus.');
      Jurusan::find($id)->delete();
      return redirect()->route('admin.jurusan.index');
    }
}
