<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateSiswaRequest;
use App\Http\Requests\SiswaRequest;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\Siswa;
use App\SiswaMeta;
use App\SiswaWali;
use App\User;
use App\Kelas;
use App\Agama;
use App\Pekerjaan;
use App\Pendidikan;
use App\Penghasilan;
use App\TahunAjaran;

use Alert;
use DB;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        $q = $request->get('q');
        $kelas_id = $request->get('kelas_id');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $kelas = Kelas::lists('kode_kelas', 'id');

        $siswa = Siswa::where('nama', 'LIKE', '%'.$q.'%')
            ->where('kelas_id', 'LIKE', '%'.$kelas_id.'%')
            ->where('tahun_ajaran', $tahun_ajaran)
            ->where('hapus', '0')
            ->orderBy('kelas_id','asc')
            ->orderBy('nama','asc')
            ->paginate(9);

        return view('admin.siswa.siswa', compact('siswa', 'no' ,'q', 'kelas'));
    }

    public function create(Request $request)
    {
        $agama = Agama::lists('agama', 'id');
        $kelas = Kelas::lists('kode_kelas', 'id');
        $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
        $penghasilan = Penghasilan::lists('penghasilan', 'penghasilan');
        $pendidikan = Pendidikan::lists('pendidikan', 'pendidikan');

        return view('admin.siswa.tambah_siswa', compact('agama','kelas','pendidikan','pekerjaan','penghasilan'));
    }

    public function store(CreateSiswaRequest $request)
    {
        // SIMPAN DATA SISWA
        $dataSiswa = $request->only('agama_id','kelas_id','no_induk','nama','tahun_ajaran','hapus');
        
        Siswa::create($dataSiswa);

        // SIMPAN DATA SISWA META
        $dataSiswaMeta = $request->only('no_induk','nisn','jk','tempat_lahir','tanggal_lahir','alamat',
        'rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp');

        if ($request->hasFile('photo')) {
            $dataSiswaMeta['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            if($request->jk == 'laki-laki'){
              $dataSiswaMeta['photo'] = 'avatar1.png';
            } else {
              $dataSiswaMeta['photo'] = 'avatar3.png';
            }
        }

        SiswaMeta::create($dataSiswaMeta);

        // SIMPAN DATA SISWA WALI
        $dataSiswaWali = $request->only('no_induk',
            'ayah_nama','ayah_tl','ayah_pekerjaan','ayah_penghasilan','ayah_pendidikan','ayah_telp',
            'ibu_nama','ibu_tl','ibu_pekerjaan','ibu_penghasilan','ibu_pendidikan','ibu_telp',
            'wali_nama','wali_tl','wali_pekerjaan','wali_penghasilan','wali_pendidikan','wali_telp');

        SiswaWali::create($dataSiswaWali);

        // SIMPAN DATA USER
        User::create([
            'no_induk'  => $request->no_induk,
            'email'     => $request->no_induk.'@smkpelitapesawaran.sch.id',
            'username'  => $request->no_induk,
            'password'  => bcrypt('smkpelita'),
            'api_token' => bcrypt($request->no_induk.'@smkpelitapesawaran.sch.id'),
            'role'      => 'siswa'
        ]);

        Alert::success('Success Message', 'Siswa dengan nama '.$request->get('nama').' telah ditambahkan.');
        // Session::flash('success', 'Siswa dengan nama '.$request->get('nama').' telah ditambahkan.');
        return redirect()->route('admin.siswa.index');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $agama = Agama::lists('agama', 'id');
        $kelas = Kelas::lists('kode_kelas', 'id');
  
        $siswa      = Siswa::findOrFail($id); 
  
        return view('admin.siswa.edit_siswa', compact('siswa','agama','kelas'));
    }

    public function update(SiswaRequest $request, $id)
    {
        // SIMPAN DATA SISWA
        $siswa = Siswa::findOrFail($id);

        $dataSiswa = $request->only('agama_id','kelas_id','no_induk','nama','tahun_ajaran','hapus');
        
        $siswa->update($dataSiswa);

        // SIMPAN DATA SISWA META
        $dataSiswaMeta = $request->only('nisn','jk','tempat_lahir','tanggal_lahir','alamat',
        'rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp');

        if ($request->hasFile('photo')) {
            $dataSiswaMeta['photo'] = $this->savePhoto($request->file('photo'));
            if (($siswa->siswa_meta->photo == 'avatar1.png')||($siswa->siswa_meta->photo == 'avatar3.png')) {

            } else {
              $this->deletePhoto($siswa->photo);
            }
        } else {
            if($siswa->photo !== ('avatar1.png')||('avatar3.png')) {
              $dataSiswaMeta['photo'] = $siswa->siswa_meta->photo;
            } elseif ($request->jk == 'laki-laki') {
              $dataSiswaMeta['photo'] = 'avatar1.png';
            } else {
              $dataSiswaMeta['photo'] = 'avatar3.png';
            }
        }

        $siswa_meta = SiswaMeta::findOrFail($id);

        $siswa_meta->update($dataSiswaMeta);

        Alert::success('Success Message', 'Siswa dengan nama '.$request->get('nama').' telah diubah.');
        // Session::flash('info', 'Siswa dengan nama '.$request->get('nama').' telah diubah.');
        return redirect()->route('admin.siswa.index');
    }

    // HAPUS DATA SISWA SEMENTARA
    public function hapus($id)
    {
        $hapus = Siswa::findOrFail($id);
  
        DB::update('update siswas set hapus = "1" where id = '.$id);
  
        return redirect()->route('admin.siswa.index');
    }

    // KEMBALIKAN DATA SISWA SEMENTARA
    public function balikin($id)
    {
        $hapus = Siswa::findOrFail($id);

        DB::update('update siswas set hapus = "0" where id = '.$id);

        return redirect()->route('admin.sampah.index');
    }

    public function destroy($id)
    {
        $hapus = Siswa::findOrFail($id);
        // Session::flash('error', 'Siswa dengan nama '.$hapus->nama.' telah dihapus permanen.');
  
        $siswa = DB::table('siswas')->where('id',$id);

        //delete siswa
        $siswa      = Siswa::find($id);
        $siswa_wali = SiswaWali::find($id);
        $siswa_meta = SiswaMeta::find($id);
  
        if ($siswa->siswa_meta->photo !== ('avatar1.png')&&('avatar3.png')) $this->deletePhoto($siswa->siswa_meta->photo);
  
        //delete siswa
        $siswa->delete();

        //delete siswa meta
        $siswa_meta->delete();
  
        //delete wali
        $siswa_wali->delete();
  
        //delete user
        $user = User::where('no_induk',$siswa->no_induk);
        $user->delete();
  
        return redirect()->route('admin.siswa.index');
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'img/siswa';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'img/siswa/'.$filename;
      return File::delete($path);
    }
}
