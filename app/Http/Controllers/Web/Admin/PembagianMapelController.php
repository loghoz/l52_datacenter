<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Pemapel;
use App\MataPelajaran;
use App\Jurusan;
use App\Guru;

class PembagianMapelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
      $tahun_ajaran = $request->cookie('tahun_ajaran');

      $q = $request->get('q');
      $page = $request->get('page');

      $no = 1;

      if($page>1){
          $no = $page * 5 - 4;
      }else{
          $no=1;
      }

      $pemap = Guru::with('pemapel','agama')
              ->where('nama','not like','-')
              ->where('nama', 'LIKE', '%'.$q.'%')
              ->where('jabatan_id', 6)
              ->where('tahun_ajaran',$tahun_ajaran)
              ->paginate(10);

      $guru = Guru::where('tahun_ajaran', $tahun_ajaran)->where('jabatan_id', 6)->lists('nama', 'id');

      $mapel = MataPelajaran::orderBy('kode_mat_pel','asc')->get();

      return view('admin.mapel.pemapel', compact('guru','mapel','no','pemap','q','tahun_ajaran'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Pemapel::create($request->all());

        // Session::flash('success', 'Pembagian Mata Pelajaran telah ditambahkan.');
        return redirect()->route('admin.mapel.pemapel.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
      Pemapel::find($id)->delete();
    //   Session::flash('error', 'Pembagian Mata Pelajaran telah dihapus.');
      return redirect()->route('admin.mapel.pemapel.index');
    }
}
