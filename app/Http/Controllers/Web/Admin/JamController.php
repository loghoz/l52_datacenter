<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Jam;

class JamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Jam::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $jam = Jam::findOrFail($id);

         return view('admin.pengaturan.edit_jam', compact('jam'));
     }

     public function update(Request $request, $id)
     {
         $jam = Jam::findOrFail($id);
         $data = $request->only('jam');
         $jam->update($data);
        //  Session::flash('info', 'Jam telah dirubah.');
         return redirect()->route('admin.pengaturan.index');
     }

    public function destroy($id)
    {
      Jam::find($id)->delete();
    //   Session::flash('error', 'Jam telah dihapus.');

      return redirect()->route('admin.pengaturan.index');
    }
}
