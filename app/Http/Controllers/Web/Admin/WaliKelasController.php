<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\WaliKelas;
use App\Guru;
use App\Kelas;

class WaliKelasController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $kelas = Kelas::lists('kode_kelas', 'id');
        $guru = Guru::where('tahun_ajaran', $tahun_ajaran)->where('jabatan_id', 6)->lists('nama', 'id');

        $walikelas = WaliKelas::where('tahun_ajaran',$tahun_ajaran)
                                ->paginate(5);
        return view('admin.walikelas.walikelas', compact('walikelas','no','guru','kelas'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        WaliKelas::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.walikelas.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $wali = WaliKelas::findOrFail($id);
        $kelas = Kelas::lists('kode_kelas', 'id');
        $guru = Guru::lists('nama', 'id');

        return view('admin.walikelas.edit_walikelas', compact('wali','kelas','guru'));
    }

    public function update(Request $request, $id)
    {
        $wali = WaliKelas::findOrFail($id);

        $wali->update($request->all());
    //   Session::flash('info', 'Jurusan '.$request->get('jurusan').' telah dirubah.');
        return redirect()->route('admin.walikelas.index');
    }

    public function destroy($id)
    {
        $hapus = WaliKelas::findOrFail($id);
        //   Session::flash('error', 'Jurusan '.$hapus->jurusan.' telah dihapus.');
        WaliKelas::find($id)->delete();
        return redirect()->route('admin.walikelas.index');
    }
}
