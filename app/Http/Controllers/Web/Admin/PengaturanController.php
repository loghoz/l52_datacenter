<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Agama;
use App\Jabatan;
use App\Jam;
use App\JumlahSoal;
use App\Pekerjaan;
use App\Pendidikan;
use App\Penghasilan;
use App\Status;
use App\TahunAjaran;

class PengaturanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {

        $agama        = Agama::where('agama','NOT LIKE','Lainnya')->paginate(5,['*'],'aga');
        $jabatan      = Jabatan::paginate(5,['*'],'jab');
        $jam          = Jam::paginate(5,['*'],'jm');
        // $jumlah_soal  = JumlahSoal::paginate(5,['*'],'jum'); //jumlah soal di return view
        $pekerjaan    = Pekerjaan::where('pekerjaan','NOT LIKE','-')
                                  ->where('pekerjaan','NOT LIKE','Lainnya')
                                  ->paginate(5,['*'],'kerja');
        $pendidikan   = Pendidikan::where('pendidikan','NOT LIKE','-')->paginate(5,['*'],'pend');
        $penghasilan  = Penghasilan::where('penghasilan','NOT LIKE','-')->paginate(5,['*'],'hasil');
        $status       = Status::paginate(5,['*'],'sta');
        $ta           = TahunAjaran::orderBy('tahun_ajaran','asc')->paginate(5,['*'],'ta');

        return view('admin.pengaturan.pengaturan', compact('agama','jabatan','jam','jumlah_soal','pekerjaan','pendidikan','penghasilan','status','ta'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
