<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\JadwalRequest;
use App\Http\Controllers\Controller;

// model
use App\Kelas;
use App\Guru;
use App\Jadwal;
use App\Hari;
use App\Pemapel;
use App\TahunAjaran;
use App\Jam;

use DB;

class JadwalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        $q = $request->get('q');
        $r = $request->get('r');

        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $jadwal = Jadwal::where('kode_kelas',$r)
              ->where('hari_id',$q)
              ->where('tahun_ajaran',$tahun_ajaran )
              ->orderBy('jam_mulai')
              ->get();


        // $jadwal = Jadwal::all();

        $h_kelas = Kelas::where('kode_kelas', $r)->select('kode_kelas')->get();
        $h_hari = Hari::where('id', $q)->select('hari')->get();

        $kelas = Kelas::lists('kode_kelas', 'kode_kelas');
        $hari = Hari::lists('hari', 'id');
        $tahun = TahunAjaran::lists('tahun_ajaran', 'id');
        $guru = Guru::where('tahun_ajaran', $tahun_ajaran)->where('jabatan_id', 6)->orWhere('jabatan_id', 1)->lists('nama', 'id');
        $jam = Jam::lists('jam', 'jam');

        return view('admin.jadwal.jadwal', compact('h_kelas','h_hari','kelas','guru','hari','tahun','jam', 'no' ,'jadwal','q','r'));
    }

    public function pemapel(Request $request, $id){

        $pemapel = DB::table('pemapels')
            ->join('mata_pelajarans', 'pemapels.mata_pelajaran_id', '=', 'mata_pelajarans.id')
            ->join('gurus', 'pemapels.guru_id', '=', 'gurus.id')
            ->select('pemapels.*','mata_pelajarans.*','gurus.*')
            ->where('pemapels.guru_id', $id)
            ->get();

        return response()->json($pemapel);
    }
  
    public function guru(Request $request){
        $tahun_ajaran = $request->cookie('tahun_ajaran');
        $h_guru = $request->get('guru');
  
        $guru = Guru::where('tahun_ajaran', $tahun_ajaran)->where('jabatan_id', 6)->orWhere('jabatan_id', 1)->lists('nama', 'id');
  
        $hasil = Jadwal::where('guru_id', $h_guru)->where('tahun_ajaran', $tahun_ajaran)->get();
        $jam = Jadwal::where('guru_id', $h_guru)->where('tahun_ajaran', $tahun_ajaran)->sum('jam_ajar');
  
        $hasila = Guru::where('id', $h_guru)->get();
        return view('admin.jadwal.guru', compact('guru','hasil','hasila','jam'));
    }
  
    public function create()
    {
        //
    }
  
      public function store(JadwalRequest $request)
      {
          Jadwal::create($request->all());
        //   Session::flash('success', 'Jadwal mengajar telah ditambahkan.');
          return redirect()->route('admin.jadwal.jadwal.index');
      }
  
      public function show($id)
      {
          //
      }
  
      public function edit($id)
      {
        $kelas = Kelas::lists('kode_kelas', 'kode_kelas');
        $hari = Hari::lists('hari', 'id');
        $tahun = TahunAjaran::lists('tahun_ajaran', 'id');
        $guru = Guru::lists('nama', 'id');
        $jam = Jam::lists('jam', 'jam');
  
        $jadwal = Jadwal::findOrFail($id);
  
        return view('admin.jadwal.edit_jadwal', compact('kelas','guru','hari','tahun','jam','jadwal'));
      }
  
      public function update(Request $request, $id)
      {
          //
      }
  
      public function destroy($id)
      {
        //   Session::flash('error', 'Jadwal telah dihapus.');
          Jadwal::find($id)->delete();
          return redirect()->route('admin.jadwal.jadwal.index');
      }

      public function hapus($id)
      {
        //   Session::flash('error', 'Jadwal telah dihapus.');
          Jadwal::find($id)->delete();
          return redirect('admin/jadwal/guru');
      }
}
