<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Siswa;

use DB;

class KotakSampahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        $page = $request->get('page');
        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $siswa = DB::table('siswas')
            ->join('siswa_metas', 'siswas.no_induk', '=', 'siswa_metas.no_induk')
            ->join('kelas', 'siswas.kelas_id', '=', 'kelas.id')
            ->join('agamas', 'siswas.agama_id', '=', 'agamas.id')
            ->select('siswas.*','siswa_metas.*','kelas.kode_kelas', 'agamas.agama')
            ->where('tahun_ajaran', $tahun_ajaran)
            ->where('hapus', '1')
            ->orderBy('kode_kelas','asc')
            ->orderBy('nama','asc')
            ->paginate(6);

        $guru = DB::table('gurus')
            ->join('guru_metas', 'gurus.no_induk', '=', 'guru_metas.no_induk')
            ->join('jabatans', 'gurus.jabatan_id', '=', 'jabatans.id')
            ->select('gurus.*','guru_metas.photo','jabatans.jabatan')
            ->where('hapus', '1')
            ->orderBy('nama','asc')
            ->paginate(6);

        return view('admin.kotak.sampah', compact('siswa','guru' ,'no'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
