<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\SiswaWaliRequest;
use App\Http\Controllers\Controller;

use App\SiswaWali;
use App\Pekerjaan;
use App\Pendidikan;
use App\Penghasilan;

class SiswaWaliController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
      $pekerjaan = Pekerjaan::lists('pekerjaan', 'pekerjaan');
      $penghasilan = Penghasilan::lists('penghasilan', 'penghasilan');
      $pendidikan = Pendidikan::lists('pendidikan', 'pendidikan');

      $siswawali = SiswaWali::findOrFail($id);

      return view('admin.siswawali.edit_siswawali', compact('siswawali','pendidikan','pekerjaan','penghasilan'));
    }

    public function update(SiswaWaliRequest $request, $id)
    {
      $siswawali = SiswaWali::findOrFail($id);

      $siswawali->update($request->all());
    //   Session::flash('info', 'Data orangtua / wali telah dirubah.');
      return redirect()->route('admin.siswa.index');
    }

    public function destroy($id)
    {
        //
    }
}
