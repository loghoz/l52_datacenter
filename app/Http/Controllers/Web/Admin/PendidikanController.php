<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pendidikan;

class PendidikanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Pendidikan::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $pendidikan = Pendidikan::findOrFail($id);

         return view('admin.pengaturan.edit_pendidikan', compact('pendidikan'));
     }

     public function update(Request $request, $id)
     {
         $pendidikan = Pendidikan::findOrFail($id);
         $data = $request->only('pendidikan');
         $pendidikan->update($data);
        //  Session::flash('info', 'Pendidikan '.$request->get('pendidikan').' telah dirubah.');
         return redirect()->route('admin.pengaturan.index');
     }

    public function destroy($id)
    {
      $hapus = Pendidikan::findOrFail($id);
    //   Session::flash('error', 'Pendidikan '.$hapus->pendidikan.' telah dihapus.');
      Pendidikan::find($id)->delete();
      return redirect()->route('admin.pengaturan.index');
    }
}
