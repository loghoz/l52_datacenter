<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Jabatan;

class JabatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Jabatan::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
         $jabatan = Jabatan::findOrFail($id);

         return view('admin.pengaturan.edit_jabatan', compact('jabatan'));
    }

    public function update(Request $request, $id)
    {
         $jabatan = Jabatan::findOrFail($id);
         $data = $request->only('jabatan');
         $jabatan->update($data);
        //  Session::flash('info', 'Jabatan '.$request->get('jabatan').' telah dirubah.');
         return redirect()->route('admin.pengaturan.index');
    }

    public function destroy($id)
    {
      $hapus = Jabatan::findOrFail($id);
    //   Session::flash('error', 'Jabatan '.$hapus->jabatan.' telah dihapus.');

      Jabatan::find($id)->delete();

      return redirect()->route('admin.pengaturan.index');
    }
}
