<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Kelas;
use App\Hari;
use App\Jadwal;
use App\Absensi;
use App\Siswa;

class AbsensiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        // CARI KELAS
        $cari_kelas = $request->get('cari_kelas');
        if (empty($cari_kelas)) {
            $absensi = "";
        } else {
            $kelas = Kelas::where('kode_kelas',$cari_kelas)->get()->first();

            $absensi = Siswa::where('kelas_id',$kelas->id)
                    ->where('tahun_ajaran',$tahun_ajaran)
                    ->orderBy('nama')
                    ->get();
        }
        
        // LIST KELAS
        $lkelas = Kelas::lists('kode_kelas', 'kode_kelas');

        // CARI HARI
        $date = date('l');
        $hari = Hari::where('day',$date)->get()->first();

        $tanggal = date('d-m-Y');

        // JADWAL
        $jadwal = Jadwal::where('kode_kelas',$cari_kelas)
                    ->where('hari_id',$hari->id)
                    ->where('tahun_ajaran',$tahun_ajaran )
                    ->orderBy('jam_mulai')
                    ->get();
            
        return view('admin.absensi.absensi', compact('jadwal','lkelas','cari_kelas','hari','tanggal','absensi'));
    }

    public function absensi(Request $request ,$id)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        // CARI HARI
        $date = date('l');
        $hari = Hari::where('day',$date)->get()->first();

        $tanggal = date('d-m-Y');

        //SISWA
        $jadwal = Jadwal::where('id',$id)
                    ->where('tahun_ajaran',$tahun_ajaran)
                    ->get()->first();

        $kelas = Kelas::where('kode_kelas',$jadwal->kode_kelas)->get()->first();
        $absen = Siswa::where('kelas_id',$kelas->id)
                    ->where('tahun_ajaran',$tahun_ajaran)
                    ->orderBy('nama')
                    ->get();

        return view('admin.absensi.isi_absen', compact('jadwal','hari','tanggal','absen'));
    }

    public function data(Request $request)
    {
        // $tahun_ajaran = $request->cookie('tahun_ajaran');

        // // CARI HARI
        // $date = date('l');
        // $hari = Hari::where('day',$date)->get()->first();

        // $tanggal = date('d-m-Y');

        return view('admin.absensi.data');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $absen = $request->get('absensi');
        $siswa = $request->get('siswa_id');

        $jumlah = count($siswa);

        $j=0;
        foreach ($absen as $urut => $kehadiran) {
                unset($absen[$urut]);
                $urut_baru=$j++;
                $absen[$urut_baru]=$kehadiran;
        }

        for($i=0; $i < $jumlah ; $i++) {
            $data = Absensi::create([ 
              'absensi'         => $absen[$i],
              'siswa_id'        => $input['siswa_id'][$i],
              'guru_id'         => $input['guru_id'][$i],
              'kelas_id'        => $input['kelas_id'][$i],
              'hari_id'         => $input['hari_id'][$i],
              'jadwal_id'       => $input['jadwal_id'][$i],
              'tanggal'         => $input['tanggal'][$i],
              'tahun_ajaran'    => $input['tahun_ajaran'][$i]
            ]);
            $data->save();            
        }

        return redirect()->route('admin.absensi.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $absen = Absensi::findOrFail($id);

        return view('admin.absensi.edit_absen', compact('absen'));
    }

    public function update(Request $request, $id)
    {
        $absen = Absensi::findOrFail($id);
        $kelas = Kelas::where('id',$absen->kelas_id)->get()->first();

        $absen->update($request->all());
        //   Session::flash('info', 'Jurusan '.$request->get('jurusan').' telah dirubah.');
        return redirect('admin/absensi?cari_kelas='.$kelas->kode_kelas);
    }

    public function destroy($id)
    {
        //
    }
}
