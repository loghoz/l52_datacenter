<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\KelasRequest;
use App\Http\Controllers\Controller;

// model Kelas
use App\Kelas;
use App\Jurusan;

class KelasController extends Controller
{
    public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('role:admin');
  }

  public function index(Request $request)
    {
        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $kelas = Kelas::where('kode_kelas', 'LIKE', '%'.$q.'%')
        ->orderBy('kode_kelas','asc')
        ->paginate(5);

        $jurusan = Jurusan::lists('jurusan', 'id');

        return view('admin.kelas.kelas', compact('kelas','no', 'q', 'jurusan'));
    }
    public function create()
    {
        //
    }

    public function store(KelasRequest $request)
    {
      Kelas::create($request->all());
    //   Session::flash('success', 'Kelas '.$request->get('kode_kelas').' telah ditambahkan.');
      return redirect()->route('admin.kelas.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $jurusan = Jurusan::lists('jurusan', 'id');
        $kelas = Kelas::findOrFail($id);

        return view('admin.kelas.edit_kelas', compact('kelas', 'jurusan'));
    }

    public function update(KelasRequest $request, $id)
    {
        $kelas = Kelas::findOrFail($id);

        $kelas->update($request->all());
        // Session::flash('info', 'Kelas '.$request->get('kode_kelas').' telah dirubah.');
        return redirect()->route('admin.kelas.index');
    }

    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        // Session::flash('error', 'Kelas '.$kelas->kode_kelas.' telah dihapus.');

        Kelas::find($id)->delete();
        return redirect()->route('admin.kelas.index');
    }
}
