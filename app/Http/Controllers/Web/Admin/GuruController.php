<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\GuruRequest;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\User;
use App\Guru;
use App\GuruMeta;
use App\Agama;
use App\Ijazah;
use App\Status;
use App\Jabatan;

use DB;

class GuruController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun = $request->cookie('tahun_ajaran');

        $q = $request->get('q');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $guru = Guru::where('no_induk', 'NOT LIKE', '999999')
            ->where('nama', 'LIKE', '%'.$q.'%')
            ->where('hapus', '0')
            ->where('tahun_ajaran',$tahun)
            ->orderBy('nama','asc')
            ->paginate(9);

        return view('admin.guru.guru', compact('guru', 'no' ,'q'));
    }

    public function create()
    {
        $agama = Agama::lists('agama', 'id');
        $ijazah = Ijazah::lists('ijazah', 'id');
        $status = Status::lists('status', 'id');
        $jabatan = Jabatan::lists('jabatan', 'id');

        return view('admin.guru.tambah_guru', compact('agama','ijazah','status', 'jabatan'));
    }

    public function store(GuruRequest $request)
    {
        // SIMPAN DATA GURU
        $dataGuru = $request->only('agama_id','jabatan_id','ijazah_id','status_id','no_induk','nama','tahun_ajaran','hapus');

        Guru::create($dataGuru);
        
        // SIMPAN DATA GURU META
        $dataGuruMeta = $request->only('no_induk','nuptk','jk','tempat_lahir','tanggal_lahir','ijazah_jurusan','tahun_tugas','alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp','photo');

        if ($request->hasFile('photo')) {
            $dataGuruMeta['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            if($request->jk == 'laki-laki'){
              $dataGuruMeta['photo'] = 'avatar5.png';
            } else {
              $dataGuruMeta['photo'] = 'avatar2.png';
            }
        }

        GuruMeta::create($dataGuruMeta);

        // SIMPAN DATA USER GURU
        User::create([
            'no_induk'  => $request->no_induk,
            'email'     => $request->no_induk.'@smkpelitapesawaran.sch.id',
            'username'  => $request->no_induk,
            'password'  => bcrypt('smkp3l1t4'),
            'api_token' => bcrypt($request->no_induk.'@smkpelitapesawaran.sch.id'),
            'role'      => 'guru'
        ]);

        // Session::flash('success', 'Guru dengan nama '.$request->get('nama').' telah ditambahkan.');
        return redirect()->route('admin.guru.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $agama = Agama::lists('agama', 'id');
        $ijazah = Ijazah::lists('ijazah', 'id');
        $status = Status::lists('status', 'id');
        $jabatan = Jabatan::lists('jabatan', 'id');

        $guru = Guru::findOrFail($id);

        return view('admin.guru.edit_guru', compact('guru', 'agama','ijazah','status', 'jabatan'));
    }

    public function update(GuruRequest $request, $id)
    {
        // SIMPAN DATA GURU
        $guru = Guru::findOrFail($id);

        $data_guru = $request->only('agama_id','jabatan_id','ijazah_id','status_id','no_induk','nama','tahun_ajaran','hapus');

        $guru->update($data_guru);

        // SIMPAN DATA GURU META
        $guru_meta = GuruMeta::findOrFail($id);

        $dataGuruMeta = $request->only('nuptk','jk','tempat_lahir','tanggal_lahir','ijazah_jurusan','tahun_tugas','alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp','photo');

        if ($request->hasFile('photo')) {
            $dataGuruMeta['photo'] = $this->savePhoto($request->file('photo'));
            if (($guru->guru_meta->photo == 'avatar2.png')||($guru->guru_meta->photo == 'avatar5.png')) {

            } else {
                $this->deletePhoto($guru->guru_meta->photo);
            }
        } else {
            if($guru->guru_meta->photo !== ('avatar2.png')||('avatar5.png')) {
                $dataGuruMeta['photo'] = $guru->guru_meta->photo;
            } elseif ($request->jk == 'laki-laki') {
                $dataGuruMeta['photo'] = 'avatar5.png';
            } else {
                $dataGuruMeta['photo'] = 'avatar2.png';
            }
        }

        $guru_meta->update($dataGuruMeta);

        return redirect()->route('admin.guru.index');
    }

    // HAPUS DATA GURU SEMENTARA
    public function hapus($id)
    {
        DB::update('update gurus set hapus = "1" where id = '.$id);
  
        return redirect()->route('admin.guru.index');
    }

    // KEMBALIKAN DATA GURU SEMENTARA
    public function balikin($id)
    {
        DB::update('update gurus set hapus = "0" where id = '.$id);

        return redirect()->route('admin.sampah.index');
    }

    public function destroy($id)
    {
        $hapus = Guru::findOrFail($id);
        // Session::flash('error', 'Guru dengan nama '.$hapus->nama.' telah dihapus permanen.');

        $guru       = Guru::findOrFail($id);
        $guru_meta  = GuruMeta::findOrFail($id);

        if ($guru->guru_meta->photo !== ('avatar2.png')&&('avatar5.png')) $this->deletePhoto($guru->guru_meta->photo);
        
        //delete user
        $user = User::where('no_induk',$guru->no_induk);
        $user->delete();

        //delete guru
        $guru->delete();

        //delete guru meta
        $guru_meta->delete();

        return redirect()->route('admin.guru.index');
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'img/guru';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'img/guru/'. $filename;
      return File::delete($path);
    }

}
