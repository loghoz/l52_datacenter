<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pekerjaan;

class PekerjaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Pekerjaan::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $pekerjaan = Pekerjaan::findOrFail($id);

         return view('admin.pengaturan.edit_pekerjaan', compact('pekerjaan'));
     }

     public function update(Request $request, $id)
     {
         $pekerjaan = Pekerjaan::findOrFail($id);
         $data = $request->only('pekerjaan');
         $pekerjaan->update($data);
        //  Session::flash('info', 'Pekerjaan '.$request->get('pekerjaan').' telah dirubah.');
         return redirect()->route('admin.pengaturan.index');
     }

    public function destroy($id)
    {
      $hapus = Pekerjaan::findOrFail($id);
    //   Session::flash('error', 'Pekerjaan '.$hapus->pekerjaan.' telah dihapus.');
      Pekerjaan::find($id)->delete();
      return redirect()->route('admin.pengaturan.index');
    }
}
