<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Penghasilan;

class PenghasilanController extends Controller
{
    public function __construct()
  {
      $this->middleware('auth');
      $this->middleware('role:admin');
  }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Penghasilan::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function show($id)
    {
        //
    }

     public function edit($id)
     {
         $penghasilan = Penghasilan::findOrFail($id);

         return view('admin.pengaturan.edit_penghasilan', compact('penghasilan'));
     }

     public function update(Request $request, $id)
     {
         $penghasilan = Penghasilan::findOrFail($id);
         $data = $request->only('penghasilan');
         $penghasilan->update($data);
        //  Session::flash('info', 'Penghasilan telah dirubah.');
         return redirect()->route('admin.pengaturan.index');
     }

     public function destroy($id)
     {
       Penghasilan::find($id)->delete();
    //    Session::flash('error', 'Penghasilan telah dihapus.');

       return redirect()->route('admin.pengaturan.index');
     }
}
