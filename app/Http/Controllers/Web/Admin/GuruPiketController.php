<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Hari;
use App\Guru;
use App\GuruPiket;

class GuruPiketController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $hari = Hari::lists('hari', 'id');
        $guru = Guru::where('tahun_ajaran', $tahun_ajaran)->where('jabatan_id', 6)->lists('nama', 'id');

        $gupit = GuruPiket::where('tahun_ajaran',$tahun_ajaran)
                            ->orderBy('hari_id','asc')
                            ->paginate(5);

        return view('admin.guru.piket', compact('gupit','no','guru','hari'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        GuruPiket::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.gurupiket.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $gupit = GuruPiket::findOrFail($id);
        $hari  = Hari::lists('hari', 'id');
        $guru  = Guru::lists('nama', 'id');

        return view('admin.guru.edit_gurupiket', compact('gupit','hari','guru'));
    }

    public function update(Request $request, $id)
    {
        $gupit = GuruPiket::findOrFail($id);

        $gupit->update($request->all());
    //   Session::flash('info', 'Jurusan '.$request->get('jurusan').' telah dirubah.');
        return redirect()->route('admin.gurupiket.index');
    }

    public function destroy($id)
    {
        $hapus = GuruPiket::findOrFail($id);
        //   Session::flash('error', 'Jurusan '.$hapus->jurusan.' telah dihapus.');
        GuruPiket::find($id)->delete();
        return redirect()->route('admin.gurupiket.index');
    }
}
