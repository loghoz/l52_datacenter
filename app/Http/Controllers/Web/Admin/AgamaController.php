<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Agama;

class AgamaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:admin');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        Agama::create($request->all());
        //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $agama = Agama::findOrFail($id);

        return view('admin.pengaturan.edit_agama', compact('agama'));
    }

    public function update(Request $request, $id)
    {
        $agama = Agama::findOrFail($id);
        $data = $request->only('agama');
        $agama->update($data);

        // Session::flash('info', 'Agama '.$request->get('agama').' telah dirubah.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function destroy($id)
    {
      $agama = Agama::findOrFail($id);
    //   Session::flash('error', 'Agama '.$agama->agama.' telah dihapus.');

      Agama::find($id)->delete();

      return redirect()->route('admin.pengaturan.index');
    }
}
