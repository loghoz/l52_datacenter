<?php

namespace App\Http\Controllers\Web\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\TahunAjaran;
use App\Guru;
use App\Siswa;
use App\WaliKelas;

class TahunAjaranController extends Controller
{

    public function index()
    {
        $ta = TahunAjaran::lists('tahun_ajaran', 'tahun_ajaran');
        
        return view('auth.tahun', compact('ta'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $tahun = $request->get('tahun_ajaran');
        
        $role = Auth::user()->role;
        $n_i = Auth::user()->no_induk;
        
        if ($role=='guru') {
            $guru = Guru::where('no_induk',$n_i)->where('tahun_ajaran', $tahun)->first();
                    
            if (empty($guru)) {
                return redirect('/tahun')->with('message', 'Tidak ada data di tahun ajaran '.$tahun);;
            }else{
                $walkel = WaliKelas::where('guru_id', $guru->id)->where('tahun_ajaran',$tahun)->get()->first();

                $photo = cookie()->forever('photo', $guru->guru_meta->photo);
                $nama = cookie()->forever('nama', $guru->nama);
                $ta = cookie()->forever('tahun_ajaran', $tahun);

                if (empty($walkel)) {
                    $walikelas = cookie()->forever('walikelas', '-');
                }else{
                    $walikelas = cookie()->forever('walikelas', $walkel->kelas_id);
                }

                return redirect()->route('guru.beranda.index')
                ->withCookie($ta)
                ->withCookie($photo)
                ->withCookie($nama)
                ->withCookie($walikelas);
            }

        } elseif ($role=='siswa') {
            $siswa = Siswa::where('no_induk',$n_i)->where('tahun_ajaran', $tahun)->first();
            
            if (empty($siswa)) {
                return redirect('/tahun')->with('message', 'Tidak ada data di tahun ajaran '.$tahun);;
            }else{
                $photo = cookie()->forever('photo', $siswa->siswa_meta->photo);
                $nama = cookie()->forever('nama', $siswa->nama);
                $ta = cookie()->forever('tahun_ajaran', $tahun);
        
                return redirect()->route('siswa.beranda.index')->withCookie($ta)->withCookie($photo)->withCookie($nama);
            }
        } else {
            $ta = cookie()->forever('tahun_ajaran', $tahun);
            $nama = cookie()->forever('nama', 'Admin');
            $photo = cookie()->forever('photo', 'avatar4.png');
        
            return redirect()->route('home.index')->withCookie($ta)->withCookie($photo)->withCookie($nama);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $ta = TahunAjaran::findOrFail($id);
        
        return view('admin.pengaturan.edit_ta', compact('ta'));
    }

    public function update(Request $request, $id)
    {
        $ta = TahunAjaran::findOrFail($id);
        $data = $request->only('tahun_ajaran');
        $ta->update($data);
        // Session::flash('info', 'Tahun Ajaran telah dirubah.');
        return redirect()->route('admin.pengaturan.index');
    }

    public function destroy($id)
    {
        TahunAjaran::find($id)->delete();
        // Session::flash('error', 'Tahun Ajaran telah dihapus.');
  
        return redirect()->route('admin.pengaturan.index');
    }

    public function tambah(Request $request)
  {
    TahunAjaran::create($request->all());
    //   Session::flash('success', 'Jurusan '.$request->get('jurusan').' telah ditambahkan.');
    return redirect()->route('admin.pengaturan.index');
  }
}
