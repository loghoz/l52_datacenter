<?php

namespace App\Http\Controllers\Web\Guru;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\GuruRequest;
use App\Http\Controllers\Controller;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

use App\User;
use App\Guru;
use App\GuruMeta;
use App\Agama;
use App\Ijazah;
use App\Status;
use App\Jabatan;

use DB;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:guru');
    }

    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $agama = Agama::lists('agama', 'id');
        $ijazah = Ijazah::lists('ijazah', 'id');
        $status = Status::lists('status', 'id');
        $jabatan = Jabatan::lists('jabatan', 'id');

        $guru = Guru::findOrFail($id);

        return view('guru.profil.edit_guru', compact('guru', 'agama','ijazah','status', 'jabatan'));
    }

    public function update(GuruRequest $request, $id)
    {
        // SIMPAN DATA GURU
        $guru = Guru::findOrFail($id);

        $data_guru = $request->only('agama_id','jabatan_id','ijazah_id','status_id','no_induk','nama','tahun_ajaran','hapus');

        $guru->update($data_guru);

        // SIMPAN DATA GURU META
        $guru_meta = GuruMeta::where('no_induk',$guru->no_induk)->get()->first();

        $dataGuruMeta = $request->only('nuptk','jk','tempat_lahir','tanggal_lahir','ijazah_jurusan','tahun_tugas','alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp','photo');

        if ($request->hasFile('photo')) {
            $dataGuruMeta['photo'] = $this->savePhoto($request->file('photo'));
            if (($guru->guru_meta->photo == 'avatar2.png')||($guru->guru_meta->photo == 'avatar5.png')) {

            } else {
                $this->deletePhoto($guru->guru_meta->photo);
            }
        } else {
            if($guru->guru_meta->photo !== ('avatar2.png')||('avatar5.png')) {
                $dataGuruMeta['photo'] = $guru->guru_meta->photo;
            } elseif ($request->jk == 'laki-laki') {
                $dataGuruMeta['photo'] = 'avatar5.png';
            } else {
                $dataGuruMeta['photo'] = 'avatar2.png';
            }
        }

        $guru_meta->update($dataGuruMeta);

        //COOKIE
        $guruCookie = Guru::where('id', $id)->get()->first();
        $guruMetaCookie = GuruMeta::where('no_induk', $guru->no_induk)->get()->first();

        $cphoto = cookie()->forever('photo', $guruMetaCookie->photo);
        $cnama = cookie()->forever('nama', $guruCookie->nama);
        
        return redirect()->route('guru.beranda.index')->withCookie($cphoto)->withCookie($cnama);
    }

    public function destroy($id)
    {
        //
    }

    protected function savePhoto(UploadedFile $photo)
    {
      $fileName = str_random(40) . '.' . $photo->guessClientExtension();
      $destinationPath = 'img/guru';
      $photo->move($destinationPath, $fileName);
      return $fileName;
    }

    public function deletePhoto($filename)
    {
      $path = 'img/guru/'. $filename;
      return File::delete($path);
    }
}
