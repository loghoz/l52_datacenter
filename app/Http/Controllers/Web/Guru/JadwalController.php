<?php

namespace App\Http\Controllers\Web\Guru;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Guru;
use App\Jadwal;

class JadwalController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:guru');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');
        $nama_guru = $request->cookie('nama');
        $guru = Guru::where('nama',$nama_guru)->get()->first();
  
        $hasil = Jadwal::where('guru_id', $guru->id)->where('tahun_ajaran', $tahun_ajaran)->get();
        $jam = Jadwal::where('guru_id', $guru->id)->where('tahun_ajaran', $tahun_ajaran)->sum('jam_ajar');

        return view('guru.jadwal.jadwal', compact('hasil','jam'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
