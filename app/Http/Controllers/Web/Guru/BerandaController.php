<?php

namespace App\Http\Controllers\Web\Guru;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Guru;
use App\Jadwal;
use App\Hari;
use App\WaliKelas;

class BerandaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:guru');
    }

    public function index(Request $request)
    {
        $tahun = $request->cookie('tahun_ajaran');
        $nama = $request->cookie('nama');
    
        $guru = Guru::where('nama',$nama)
                        ->where('tahun_ajaran',$tahun)
                        ->get();
        
        $guru_id = Guru::where('nama',$nama)->get()->first();

        $walkel = WaliKelas::where('guru_id', $guru_id->id)->where('tahun_ajaran',$tahun)->get();

        $date = date('l');
        $hari = Hari::where('day',$date)->get()->first();

        $jadwal = Jadwal::where('guru_id', $guru_id->id)
                        ->where('hari_id', $hari->id)
                        ->where('tahun_ajaran', $tahun)
                        ->get();

        return view('guru.beranda', compact('guru','jadwal','walkel'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
