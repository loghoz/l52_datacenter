<?php

namespace App\Http\Controllers\Web\Guru;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Kelas;
use App\Siswa;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:guru');
    }

    public function index(Request $request)
    {
        $tahun_ajaran = $request->cookie('tahun_ajaran');

        $q = $request->get('q');
        $kelas_id = $request->get('kelas_id');
        $page = $request->get('page');

        $no = 1;

        if($page>1){
            $no = $page * 5 - 4;
        }else{
            $no=1;
        }

        $kelas = Kelas::lists('kode_kelas', 'id');

        $siswa = Siswa::where('nama', 'LIKE', '%'.$q.'%')
            ->where('kelas_id', 'LIKE', '%'.$kelas_id.'%')
            ->where('tahun_ajaran', $tahun_ajaran)
            ->where('hapus', '0')
            ->orderBy('kelas_id','asc')
            ->orderBy('nama','asc')
            ->paginate(9);

        return view('guru.siswa.siswa', compact('siswa', 'no' ,'q', 'kelas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
