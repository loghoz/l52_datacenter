<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GuruRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
     {
         switch ($this->method()) {
             case 'POST':
             {
                 return [
                     'no_induk'  => 'required|numeric|unique:gurus',
                     'nama' => 'required|max:50|regex:/^[A-Za-z \t]*$/i',
                     'jk' => 'required',
                     'tempat_lahir' => 'required',
                     'tanggal_lahir' => 'required',
                     'agama_id' => 'required',
                     'nuptk' => 'numeric',
                     'tahun_tugas' => 'required|numeric',
                     'ijazah_jurusan' => 'required',

                     'alamat' => 'required',
                     'rt' => 'required|numeric',
                     'rw' => 'required|numeric',
                     'kecamatan' => 'required',
                     'desa' => 'required',
                     'kabupaten' => 'required',
                     'provinsi' => 'required',
                     'kode_pos' => 'numeric',
                     'telp' => 'numeric',
                     'photo' => 'image|between:0,1024|mimes:jpeg,jpg,png'
                 ];
             }

             case 'PUT':
             case 'PATCH':
             {
                 return [
                    'no_induk'  => 'required|numeric',
                    'nama' => 'required|max:50|regex:/^[A-Za-z \t]*$/i',
                    'jk' => 'required',
                    'tempat_lahir' => 'required',
                    'tanggal_lahir' => 'required',
                    'agama_id' => 'required',
                    'nuptk' => 'numeric',
                    'tahun_tugas' => 'required|numeric',
                    'ijazah_jurusan' => 'required',

                    'alamat' => 'required',
                    'rt' => 'required|numeric',
                    'rw' => 'required|numeric',
                    'kecamatan' => 'required',
                    'desa' => 'required',
                    'kabupaten' => 'required',
                    'provinsi' => 'required',
                    'kode_pos' => 'numeric',
                    'telp' => 'numeric',
                    'photo' => 'image|between:0,1024|mimes:jpeg,jpg,png'
                 ];
             }

             default:break;
         }
     }
     public function messages()
     {
         return [

             'no_induk.required' => 'Tidak boleh kosong',
             'no_induk.unique' => 'no_induk sudah ada',
             'no_induk.numeric' => 'Harus angka',
             'no_induk.max' => 'Maksimal 5 angka',

             'nama.required' => 'Tidak boleh kosong',
             'nama.max' => 'Maksimal 50 karakter',
             'nama.regex' => 'Tidak boleh terdapat angka dan simbol',

             'jk.required' => 'Pilih salah satu',
             'tempat_lahir.required' => 'Tidak boleh kosong',
             'tanggal_lahir.required' => 'Tidak boleh kosong',

             'nuptk.numeric' => 'Harus angka',

             'tahun_tugas.required' => 'Tidak boleh kosong',
             'tahun_tugas.numeric' => 'Harus angka',

             'ijazah_jurusan.required' => 'Tidak boleh kosong',

             'alamat.required' => 'Tidak boleh kosong',

             'rt.required' => 'Tidak boleh kosong',
             'rt.numeric' => 'Harus angka',
             'rw.required' => 'Tidak boleh kosong',
             'rw.numeric' => 'Harus angka',

             'kecamatan.required' => 'Tidak boleh kosong',
             'desa.required' => 'Tidak boleh kosong',
             'kabupaten.required' => 'Tidak boleh kosong',
             'provinsi.required' => 'Tidak boleh kosong',

             'kode_pos.numeric' => 'Harus angka',
             'telp.numeric' => 'Harus angka',

             'photo.image' => 'File harus format .jpg .png .jpeg',
             'photo.mimes' => 'File harus format .jpg .png .jpeg',
             'photo.between' => 'Foto harus berukuran 100kb s.d 1Mb'
         ];
     }
}
