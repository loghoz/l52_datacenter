<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AkunRequest extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
     {
         switch ($this->method()) {
             case 'POST':
             {
                 return [
                     'no_induk'  => 'required|numeric|unique:users',
                     'username' => 'required|unique:users',
                     'password' => 'required'
                 ];
             }

             case 'PUT':
             case 'PATCH':
             {
                 return [
                     'no_induk'  => 'required|numeric|unique:users',
                     'username' => 'required|unique:users',
                     'password' => 'required'
                 ];
             }

             default:break;
         }
     }
     public function messages()
     {
         return [

             'no_induk.required' => 'Tidak boleh kosong',
             'no_induk.numeric' => 'Harus angka',
             'no_induk.unique' => 'No Induk sudah ada',

             'username.required' => 'Tidak boleh kosong',
             'username.unique' => 'Username sudah terpakai',

             'password.required' => 'Tidak boleh kosong'
         ];
     }
}
