<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AkunEditRequest extends Request
{
  public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'username' => 'required|unique:users'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                   'username' => 'required|unique:users'
               ];
           }

           default:break;
       }
   }
   public function messages()
   {
       return [
           'username.required' => 'Tidak boleh kosong',
           'username.unique' => 'Username sudah terpakai'
       ];
   }
}
