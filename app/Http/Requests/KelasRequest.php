<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class KelasRequest extends Request
{
  public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'tingkat'  => 'required',
                   'kode_kelas' => 'required'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                   'tingkat'  => 'required',
                   'kode_kelas' => 'required'
               ];
           }

           default:break;
       }
   }
   public function messages()
   {
       return [

           'tingkat.required' => 'Pilih salah satu',
           'kode_kelas.required' => 'Tidak boleh kosong'
       ];
   }
}
