<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SiswaWaliRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      switch ($this->method()) {
          case 'POST':
          {
              return [
                  'ayah_tl' => 'numeric',
                  'ibu_tl' => 'numeric',
                  'wali_tl' => 'numeric',
                  'ayah_telp' => 'numeric',
                  'ibu_telp' => 'numeric',
                  'wali_telp' => 'numeric'
              ];
          }

          case 'PUT':
          case 'PATCH':
          {
              return [
                  'ayah_tl' => 'numeric',
                  'ibu_tl' => 'numeric',
                  'wali_tl' => 'numeric',
                  'ayah_telp' => 'numeric',
                  'ibu_telp' => 'numeric',
                  'wali_telp' => 'numeric'
              ];
          }

          default:break;
      }
    }

    public function messages()
    {
        return [
            'ayah_tl.numeric' => 'Harus angka',
            'ayah_telp.numeric' => 'Harus angka',

            'ibu_tl.numeric' => 'Harus angka',
            'ibu_telp.numeric' => 'Harus angka',

            'wali_tl.numeric' => 'Harus angka',
            'wali_telp.numeric' => 'Harus angka'
        ];
    }
}
