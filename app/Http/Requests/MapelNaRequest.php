<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MapelNaRequest extends Request
{
  public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'kode_mat_pel'  => 'required|unique:mata_pelajarans',
                   'mata_pelajaran' => 'required'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                   'kode_mat_pel'  => 'required',
                   'mata_pelajaran' => 'required'
               ];
           }

           default:break;
       }
   }
   public function messages()
   {
       return [

           'kode_mat_pel.required' => 'Tidak boleh kosong',
           'kode_mat_pel.unique' => 'Kode tersebut sudah ada',

           'mata_pelajaran.required' => 'Tidak boleh kosong'
       ];
   }
}
