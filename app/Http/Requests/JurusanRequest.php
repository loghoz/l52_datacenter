<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JurusanRequest extends Request
{
  public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'bidang_keahlian'  => 'required',
                   'jurusan' => 'required',
                   'kode_jurusan' => 'required'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                   'bidang_keahlian'  => 'required',
                   'jurusan' => 'required',
                   'kode_jurusan' => 'required'
               ];
           }

           default:break;
       }
   }
   public function messages()
   {
       return [

           'bidang_keahlian.required' => 'Tidak boleh kosong',
           'jurusan.required' => 'Tidak boleh kosong',
           'kode_jurusan.required' => 'Tidak boleh kosong'
       ];
   }
}
