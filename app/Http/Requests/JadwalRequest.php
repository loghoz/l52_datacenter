<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JadwalRequest extends Request
{
  public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'jam_ajar'  => 'required|numeric'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                   'jam_ajar'  => 'required|numeric'
               ];
           }

           default:break;
       }
   }
   public function messages()
   {
       return [

           'jam_ajar.required' => 'Tidak boleh kosong',
           'jam_ajar.numeric' => 'Harus angka'
       ];
   }
}
