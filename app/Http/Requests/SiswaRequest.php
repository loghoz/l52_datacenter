<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SiswaRequest extends Request
{
  public function authorize()
  {
      return true;
  }

  public function rules()
   {
       switch ($this->method()) {
           case 'POST':
           {
               return [
                   'no_induk'  => 'required|numeric',
                   'nama' => 'required|max:50|regex:/^[A-Za-z \t]*$/i',
                   'jk' => 'required',
                   'tempat_lahir' => 'required',
                   'tanggal_lahir' => 'required',
                   'agama_id' => 'required',
                   'nisn' => 'required|numeric',
                   'kelas_id' => 'required',

                   'alamat' => 'required',
                   'rt' => 'required|numeric',
                   'rw' => 'required|numeric',
                   'desa' => 'required',
                   'kecamatan' => 'required',
                   'kabupaten' => 'required',
                   'provinsi' => 'required',
                   'kode_pos' => 'numeric',
                   'telp' => 'numeric',
                   'photo' => 'image|between:0,1024|mimes:jpeg,jpg,png',

                   'ayah_tl' => 'numeric',
                   'ibu_tl' => 'numeric',
                   'wali_tl' => 'numeric',
                   'ayah_telp' => 'numeric',
                   'ibu_telp' => 'numeric',
                   'wali_telp' => 'numeric'
               ];
           }

           case 'PUT':
           case 'PATCH':
           {
               return [
                'no_induk'  => 'required|numeric',
                'nama' => 'required|max:50|regex:/^[A-Za-z \t]*$/i',
                'jk' => 'required',
                'tempat_lahir' => 'required',
                'tanggal_lahir' => 'required',
                'agama_id' => 'required',
                'nisn' => 'required|numeric',
                'kelas_id' => 'required',

                'alamat' => 'required',
                'rt' => 'required|numeric',
                'rw' => 'required|numeric',
                'desa' => 'required',
                'kecamatan' => 'required',
                'kabupaten' => 'required',
                'provinsi' => 'required',
                'kode_pos' => 'numeric',
                'telp' => 'numeric',
                'photo' => 'image|between:0,1024|mimes:jpeg,jpg,png',

                'ayah_tl' => 'numeric',
                'ibu_tl' => 'numeric',
                'wali_tl' => 'numeric',
                'ayah_telp' => 'numeric',
                'ibu_telp' => 'numeric',
                'wali_telp' => 'numeric'
               ];
           }

           default:break;
       }
   }
   public function messages()
   {
       return [

           'no_induk.required' => 'Tidak boleh kosong',
           'no_induk.unique' => 'NIS sudah ada',
           'no_induk.numeric' => 'Harus angka',
           'no_induk.max' => 'Maksimal 5 angka',

           'nama.required' => 'Tidak boleh kosong',
           'nama.max' => 'Maksimal 50 karakter',
           'nama.regex' => 'Tidak boleh terdapat angka dan simbol',

           'jk.required' => 'Pilih salah satu',
           'tempat_lahir.required' => 'Tidak boleh kosong',
           'tanggal_lahir.required' => 'Tidak boleh kosong',

           'nisn.required' => 'Tidak boleh kosong',
           'nisn.numeric' => 'Harus angka',

           'alamat.required' => 'Tidak boleh kosong',

           'rt.required' => 'Tidak boleh kosong',
           'rt.numeric' => 'Harus angka',
           'rw.required' => 'Tidak boleh kosong',
           'rw.numeric' => 'Harus angka',

           'desa.required' => 'Tidak boleh kosong',
           'kecamatan.required' => 'Tidak boleh kosong',
           'kabupaten.required' => 'Tidak boleh kosong',
           'provinsi.required' => 'Tidak boleh kosong',

           'kode_pos.numeric' => 'Harus angka',
           'telp.numeric' => 'Harus angka',

           'photo.image' => 'File harus format .jpg .png .jpeg',
           'photo.mimes' => 'File harus format .jpg .png .jpeg',
           'photo.between' => 'Foto harus berukuran 100kb s.d 1Mb',

           'ayah_tl.numeric' => 'Harus angka',
           'ayah_telp.numeric' => 'Harus angka',

           'ibu_tl.numeric' => 'Harus angka',
           'ibu_telp.numeric' => 'Harus angka',

           'wali_tl.numeric' => 'Harus angka',
           'wali_telp.numeric' => 'Harus angka'
       ];
   }
}
