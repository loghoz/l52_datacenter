<?php

Route::auth();
Route::get('/', 'HomeController@index');

//cookie tahun
Route::resource('tahun', 'Web\Admin\TahunAjaranController');
Route::resource('home', 'HomeController');
Route::resource('admin/beranda', 'Web\Admin\BerandaController');

// jurusan
Route::resource('admin/jurusan', 'Web\Admin\JurusanController');
Route::resource('admin/kelas', 'Web\Admin\KelasController');

//siswa
Route::resource('admin/siswa', 'Web\Admin\SiswaController');
Route::resource('admin/siswa/hapus', 'Web\Admin\SiswaController@hapus');
Route::resource('admin/siswa/balikin', 'Web\Admin\SiswaController@balikin');
Route::resource('admin/siswawali', 'Web\Admin\SiswaWaliController');
// Route::post('admin/siswa/upload', 'Admin\SiswaController@postSiswa'); //upload data lewat excel

//guru
Route::resource('admin/guru', 'Web\Admin\GuruController');
Route::resource('admin/guru/hapus', 'Web\Admin\GuruController@hapus');
Route::resource('admin/guru/balikin', 'Web\Admin\GuruController@balikin');
Route::resource('admin/walikelas', 'Web\Admin\WaliKelasController');
Route::resource('admin/gurupiket', 'Web\Admin\GuruPiketController');

//kotak sampah
Route::resource('admin/sampah', 'Web\Admin\KotakSampahController');

//mata pelajaran
Route::resource('admin/mapel/na', 'Web\Admin\MapelNaController');
Route::resource('admin/mapel/pro', 'Web\Admin\MapelProController');
Route::resource('admin/mapel/pemapel', 'Web\Admin\PembagianMapelController');

//jadwal pelajaran
Route::resource('admin/jadwal/jadwal', 'Web\Admin\JadwalController');
Route::get('admin/jadwal/pemapel/{id}','Web\Admin\JadwalController@pemapel');
Route::resource('admin/jadwal/guru', 'Web\Admin\JadwalController@guru');
Route::resource('admin/jadwal/guru/hapus', 'Web\Admin\JadwalController@hapus');

//akun
Route::resource('admin/akun/admin', 'Web\Admin\AkunController');
Route::resource('admin/akun/guru', 'Web\Admin\AkunGuruController');
Route::resource('admin/akun/siswa', 'Web\Admin\AkunSiswaController');

//Pegaturan
Route::resource('admin/pengaturan', 'Web\Admin\PengaturanController');
Route::resource('admin/agama', 'Web\Admin\AgamaController');
Route::resource('admin/jam', 'Web\Admin\JamController');
Route::resource('admin/jabatan', 'Web\Admin\JabatanController');
Route::resource('admin/pekerjaan', 'Web\Admin\PekerjaanController');
Route::resource('admin/pendidikan', 'Web\Admin\PendidikanController');
Route::resource('admin/penghasilan', 'Web\Admin\PenghasilanController');
Route::resource('admin/status', 'Web\Admin\StatusController');
Route::post('admin/ta', 'Web\Admin\TahunAjaranController@tambah');
// Route::resource('admin/jumlah', 'Admin\JumlahSoalController');

//Absensi
Route::resource('admin/absensi', 'Web\Admin\AbsensiController');
Route::resource('admin/absensi/absensi/{id}', 'Web\Admin\AbsensiController@absensi');
Route::resource('admin/absensi/data', 'Web\Admin\AbsensiController@data');

//ujian
// Route::resource('admin/ujian/midsega', 'Admin\UjianMidsegaController');
// Route::resource('admin/ujian/midsege', 'Admin\UjianMidsegeController');
// Route::resource('admin/ujian/ujisega', 'Admin\UjianSegaController');
// Route::resource('admin/ujian/ujisege', 'Admin\UjianSegeController');

// Route::get('admin/ujian/pemapel/{id}','Admin\UjianMidsegaController@pemapel');
// Route::get('admin/ujian/mapel/{id}','Admin\UjianMidsegaController@mapel');
// Route::get('admin/ujian/jadwal/{id}/{mapel}','Admin\UjianMidsegaController@jadwal');

// Route::resource('admin/ujian/soal', 'Admin\SoalController');
// Route::resource('admin/ujian/nilai', 'Admin\NilaiController');
// Route::post('admin/ujian/nilai/cetak', 'Admin\NilaiController@getPDF');
// Route::get('admin/ujian/matapel/{id}','Admin\NilaiController@jadwal');

//NaikKelas
// Route::resource('admin/naikkelas', 'Admin\NaikKelasController');




//      GGGGGGGGGGGG   UUU     UUU   RRRRRRR       UUU     UUU
//      GGGGGGGGGGGG   UUU     UUU   RRRRRRRR      UUU     UUU
//      GGG      GGG   UUU     UUU   RRR   RRR     UUU     UUU
//      GGG      GGG   UUU     UUU   RRR   RRR     UUU     UUU
//      GGG            UUU     UUU   RRR RRRR      UUU     UUU
//      GGG   GGGGGG   UUU     UUU   RRRRRRR       UUU     UUU
//      GGG   GGGGGG   UUU     UUU   RRRRRR        UUU     UUU
//      GGG      GGG   UUU     UUU   RRR  RRR      UUU     UUU
//      GGG      GGG   UUU     UUU   RRR   RRR     UUU     UUU
//      GGGGGGGGGGGG   UUUUUUUUUUU   RRR    RRR    UUUUUUUUUUU
//      GGGGGGGGGGGG   UUUUUUUUUUU   RRR     RRR   UUUUUUUUUUU



//profil
Route::resource('guru/beranda', 'Web\Guru\BerandaController');
Route::resource('guru/profil', 'Web\Guru\ProfilController');

//siswa
Route::resource('guru/siswa', 'Web\Guru\SiswaController');

//jadwal
Route::resource('guru/jadwal', 'Web\Guru\JadwalController');

//wali
Route::resource('guru/walikelas/siswa', 'Web\Guru\WaliSiswaController');

//pengaturan
// Route::resource('guru/pengaturan', 'Guru\PengaturanController');
// Route::resource('guru/akun', 'Guru\AkunController');

//ujian
// Route::resource('guru/ujian/soal', 'Guru\SoalController');
// Route::resource('guru/ujian/nilai', 'Guru\NilaiController');
// Route::post('guru/ujian/nilai/cetak', 'Guru\NilaiController@getPDF');
// Route::get('guru/ujian/matapel/{id}','Guru\NilaiController@jadwal');



//      SSSSSSSSSSSS  IIII   SSSSSSSSSSSS  WWW           WWW   AAAAAAAAAAAA
//      SSSSSSSSSSSS  IIII   SSSSSSSSSSSS  WWW           WWW   AAAAAAAAAAAA
//      SSS      SSS  IIII   SSS      SSS  WWW           WWW   AAA      AAA
//      SSS           IIII   SSS           WWW           WWW   AAA      AAA
//      SSSSSSSSSSSS  IIII   SSSSSSSSSSSS  WWW    WWW    WWW   AAAAAAAAAAAA
//      SSSSSSSSSSSS  IIII   SSSSSSSSSSSS  WWW   WWWWW   WWW   AAAAAAAAAAAA
//               SSS  IIII            SSS  WWW  WWWWWWW  WWW   AAA      AAA
//      SSS      SSS  IIII   SSS      SSS  WWW WWW   WWW WWW   AAA      AAA
//      SSSSSSSSSSSS  IIII   SSSSSSSSSSSS  WWWWWW     WWWWWW   AAA      AAA
//      SSSSSSSSSSSS  IIII   SSSSSSSSSSSS  WWWWWW     WWWWWW   AAA      AAA


//profil
// Route::resource('siswa/profil', 'Siswa\ProfilController');
// Route::resource('siswa/profil/wali', 'Siswa\WaliController');

//pengaturan
// Route::resource('siswa/pengaturan', 'Siswa\PengaturanController');
// Route::resource('siswa/akun', 'Siswa\AkunController');

//jadwal
// Route::resource('siswa/jadwal', 'Siswa\JadwalController');

//nilai
// Route::resource('siswa/nilai/midsega', 'Siswa\NilaiMidsegaController');
// Route::resource('siswa/nilai/midsege', 'Siswa\NilaiMidsegeController');
// Route::resource('siswa/nilai/ujisega', 'Siswa\NilaiSegaController');
// Route::resource('siswa/nilai/ujisege', 'Siswa\NilaiSegeController');

//ujian
// Route::resource('siswa/ujian/midsega', 'Siswa\UjianMidsegaController');
// Route::resource('siswa/ujian/midsege', 'Siswa\UjianMidsegeController');
// Route::resource('siswa/ujian/ujisega', 'Siswa\UjianSegaController');
// Route::resource('siswa/ujian/ujisege', 'Siswa\UjianSegeController');

// Route::get('siswa/ujian/midsega/ujian/{id}', 'Siswa\UjianMidsegaController@ujian');
// Route::get('siswa/ujian/midsege/ujian/{id}', 'Siswa\UjianMidsegeController@ujian');
// Route::get('siswa/ujian/ujisega/ujian/{id}', 'Siswa\UjianSegaController@ujian');
// Route::get('siswa/ujian/ujisege/ujian/{id}', 'Siswa\UjianSegeController@ujian');

// Route::resource('siswa/ujian/exam/midsega', 'Siswa\ExamMidsegaController');
// Route::resource('siswa/ujian/exam/midsege', 'Siswa\ExamMidsegeController');
// Route::resource('siswa/ujian/exam/ujisega', 'Siswa\ExamSegaController');
// Route::resource('siswa/ujian/exam/ujisege', 'Siswa\ExamSegeController');

// Route::get('siswa/ujian/exam/jawaban/{id}/{jawab}','Siswa\ExamMidsegaController@jawab');