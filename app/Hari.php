<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hari extends Model
{
    protected $table = 'haris';
   
    protected $fillable = [
        'hari','day'
    ];

    //relasi one to many ke jadwal
    public function jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }

    //relasi one to many ke absensi
    public function absensi()
    {
        return $this->hasMany('App\Absensi');
    }
}
