<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    protected $table = 'absensis';
        
    protected $fillable = [
        'guru_id','siswa_id','jadwal_id','hari_id','kelas_id','tanggal','absensi','tahun_ajaran',
    ];

    //relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }

    //relasi ke siswa
    public function siswa()
    {
        return $this->belongsTo('App\Siswa', 'siswa_id');
    }

    //relasi ke jadwal
    public function jadwal()
    {
        return $this->belongsTo('App\Jadwal', 'jadwal_id');
    }

    //relasi ke hari
    public function hari()
    {
        return $this->belongsTo('App\Hari', 'hari_id');
    }

    //relasi ke kelas
    public function kelas()
    {
        return $this->belongsTo('App\Kelas', 'kelas_id');
    }
}
