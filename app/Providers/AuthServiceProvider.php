<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('admin-access', function($user) {
            return $user->role == 'admin';
        });

        $gate->define('tu-access', function($user) {
            return $user->role == 'tu';
        });

        $gate->define('wakajur-access', function($user) {
            return $user->role == 'wakajur';
        });

        $gate->define('guru-access', function($user) {
            return $user->role == 'guru';
        });

        $gate->define('siswa-access', function($user) {
            return $user->role == 'siswa';
        });
    }
}
