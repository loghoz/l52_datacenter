<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuruMeta extends Model
{
    protected $table = 'guru_metas';
    protected $primaryKey = 'no_induk';
    protected $fillable = [
       'no_induk','nuptk','jk','tempat_lahir','tanggal_lahir','ijazah_jurusan',
       'tahun_tugas','alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos',
       'telp','photo',
    ];

    //relasi ke guru
    public function guru()
    {
        return $this->hasOne('App\Guru', 'no_induk');
    }
}
