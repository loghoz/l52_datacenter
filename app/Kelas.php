<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = 'kelas';
    protected $with = ['jurusan'];
    protected $fillable = [
        'jurusan_id','kode_kelas','tingkat','aktif',
    ];

    // relasi ke jurusan
    public function jurusan()
    {
        return $this->belongsTo('App\Jurusan', 'jurusan_id');
    }

    //relasi one to one ke wali kelas
    public function wali_kelas()
    {
        return $this->hasOne('App\WaliKelas');
    }

    //relasi one to many ke absensi
    public function absensi()
    {
        return $this->hasMany('App\Absensi');
    }
}
