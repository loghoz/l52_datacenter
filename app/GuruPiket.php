<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuruPiket extends Model
{
    protected $table = 'guru_pikets';
    
    protected $fillable = [
        'guru_id','hari_id','tahun_ajaran',
    ];

    //relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }

    //relasi ke hari
    public function hari()
    {
        return $this->belongsTo('App\Hari', 'hari_id');
    }
}
