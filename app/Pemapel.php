<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pemapel extends Model
{
    protected $table = 'pemapels';
    protected $with = ['matpel'];
    protected $fillable = [
        'guru_id','mata_pelajaran_id','tahun_ajaran',
    ];

    //relasi ke guru
    public function guru()
    {
        return $this->belongsTo('App\Guru', 'guru_id');
    }

    //relasi ke matpel
    public function matpel()
    {
        return $this->belongsTo('App\MataPelajaran', 'mata_pelajaran_id');
    }
}
