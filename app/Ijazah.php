<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ijazah extends Model
{
    protected $table = 'ijazahs';
    
    protected $fillable = [
        'ijazah',
    ];

    // relasi one to many ke guru
    public function guru()
    {
        return $this->hasMany('App\Guru');
    }
}
