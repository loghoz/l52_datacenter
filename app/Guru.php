<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    protected $table = 'gurus';
    
    protected $with = ['guru_meta','jabatan','ijazah','status','agama'];

    protected $fillable = [
        'no_induk','agama_id','jabatan_id','ijazah_id','nama','status_id','tahun_ajaran','hapus',
    ];

    //relasi one to one ke guru meta
    public function guru_meta()
    {
        return $this->belongsTo('App\GuruMeta','no_induk');
    }

    //relasi ke jabatan
    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan', 'jabatan_id');
    }

    //relasi ke ijazah
    public function ijazah()
    {
        return $this->belongsTo('App\Ijazah', 'ijazah_id');
    }

    //relasi ke agama
    public function agama()
    {
        return $this->belongsTo('App\Agama', 'agama_id');
    }

    //relasi ke status
    public function status()
    {
        return $this->belongsTo('App\Status', 'status_id');
    }

    //relasi one to one ke wali kelas
    public function wali_kelas()
    {
        return $this->hasOne('App\WaliKelas');
    }

    //relasi one to many ke jadwal
    public function jadwal()
    {
        return $this->hasMany('App\Jadwal');
    }

    //relasi one to many ke pemapel
    public function pemapel()
    {
        return $this->hasMany('App\Pemapel');
    }

    //relasi one to many ke absensi
    public function absensi()
    {
        return $this->hasMany('App\Absensi');
    }
}
