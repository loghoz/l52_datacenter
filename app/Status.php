<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'statuses';

    protected $fillable = [
        'status',
    ];

    // relasi one to many ke guru
    public function guru()
    {
        return $this->hasMany('App\Guru');
    }
}
