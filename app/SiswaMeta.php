<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiswaMeta extends Model
{
    protected $table = 'siswa_metas';
    protected $primaryKey = 'no_induk';

    protected $fillable = ['no_induk','nisn','jk','tempat_lahir','tanggal_lahir','alamat','rt','rw','desa','kecamatan','kabupaten','provinsi','kode_pos','telp','photo'];

    //relasi ke siswa
    public function siswa()
    {
        return $this->hasOne('App\Siswa', 'no_induk');
    }
}
