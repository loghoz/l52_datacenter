<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agama extends Model
{
    protected $fillable = [
        'agama',
    ];

    // relasi one to many ke guru
    public function guru()
    {
        return $this->hasMany('App\Guru');
    }

    //relasi one to one ke siswa wali
    public function siswa()
    {
        return $this->hasOne('App\Siswa');
    }
}
