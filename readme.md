## About Data Center

Sebuah Sistem Data Center untuk Sekolah SMK PELITA Gedongtataan

## Concept

Konsep sistem ini dibangun menggunakan Laravel sebagai backend system dan admin panel.

## Requirment
- App Visual Studio Code untuk ngoding
- AdminLTE 2.4.2 (Halaman Admin)
- Install NodeJS v.7 (Include NPM v.4)
- [npm](https://www.npmjs.com/) => tidak wajib
- [composer](https://getcomposer.org/download/) => wajib
- webserver (apache2, ngix) => tidak wajib pada fase Development, karena bisa menggunakan servis bawaan laravel.
- PHP 5.6 ^ => wajib
- mysql server (mysql 5.5 ^) => wajib
> npm kan menjadi wajib ketika nanti ada paket yang diinstal melalui npm, dan wajib ketika anda ingin menggunakan laravel mix.

## Package Installed

Dalam proyek ini sudah ada beberapa paket yang saya install untuk kebutuhan pengembangan sistem, diantarnya:

* CollectiveHtml - Sudah
* Socialite 
* Debugbar - Sudah
* JWT auth
* Fractal
* Faker Factory
> Setiap anggota tim yang menambahkan paket, harap edit file readme ini dan tambahkan dalam list.

## Installation
- kloning repositori ini dengan perintah `git clone https://loghoz@bitbucket.org/loghoz/laravel_datacenter_5_2.git`. Anda juga bisa menggunakan SSH clone, namun tambahkan terlebih dahulu ssh key anda ke bitbucket.
- masuk ke direktori `cd datacenter`
- lakukan instalasi paket larvel dengan perintah `composer install`
- lakukan instalasi paket Nodejs dengan perintah `npm install`. (Jika sudah menginstal npm, jika belom abaikan)
- duplikat dan ubah nama file .env.example ke env. Linux atau Mac dapet mengikuti perintah ini `cp .env.example .env`
- `php artisan key:generate` untuk mendapatkan key app
- `php artisan storage:link` untuk membuat shortcut storage file
- `php artisan migrate` untuk memigrasikan blueprint database
- `php artisan db:seed` untuk mengimport data dummy ke database
- `gulp` untuk mengkompilasi semua asset file css dan js, `jika gulp mengalami error abaikan saja.`


## Setting database

Buka file .env lalu isi sesuai dengan dengan role database anda

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=


## Run App
Error npm install
>npm config set registry http://registry.npmjs.org/

Terakhir jalankan perintah `php artisan serve` untuk menjalankan servis Laravel.
> jika proyek ini anda masukan dalam /var/www atau htdocs, anda tidak perlu menjalankan perintah di atas.

##Error Solution

- The stream or file "/var/www/html/laravel" could not be opened: failed to open stream: Permission denied

> php artisan cache:clear 
> chmod 777 storage

atau

> chmod -R 755 storage bootstrap/cache

- Open as root visual code
> sudo code --user-data-dir="~/.vscode-root"

- error /laravel/public/api/users was not found on this server.
> a2enmod rewrite  

atau

> <Directory "/var/www/html/">
> Allowoverride All
> </Directory>
