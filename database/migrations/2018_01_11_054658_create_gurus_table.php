<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGurusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gurus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agama_id')->unsigned();
            $table->integer('jabatan_id')->unsigned();
            $table->integer('ijazah_id')->unsigned();
            $table->integer('status_id')->unsigned();
            $table->string('no_induk');
            $table->string('nama');
            $table->string('tahun_ajaran');
            $table->string('hapus');
            $table->timestamps();

            $table->foreign('agama_id')
                ->references('id')
                ->on('agamas')
                ->onDelete('CASCADE');

            $table->foreign('jabatan_id')
                ->references('id')
                ->on('jabatans')
                ->onDelete('CASCADE');

            $table->foreign('ijazah_id')
                ->references('id')
                ->on('ijazahs')
                ->onDelete('CASCADE');

            $table->foreign('status_id')
                ->references('id')
                ->on('statuses')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gurus');
    }
}
