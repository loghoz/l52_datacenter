<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaliKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wali_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guru_id')->unsigned();
            $table->integer('kelas_id')->unsigned();
            $table->string('tahun_ajaran');
            $table->timestamps();

            $table->foreign('guru_id')
                ->references('id')
                ->on('gurus')
                ->onDelete('CASCADE');

            $table->foreign('kelas_id')
                ->references('id')
                ->on('kelas')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wali_kelas');
    }
}
