<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbsensisTable extends Migration
{

    public function up()
    {
        Schema::create('absensis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('guru_id')->unsigned();
            $table->integer('siswa_id')->unsigned();
            $table->integer('jadwal_id')->unsigned();
            $table->integer('kelas_id')->unsigned();
            $table->integer('hari_id')->unsigned();
            $table->string('tanggal');
            $table->string('absensi');
            $table->string('tahun_ajaran');
            $table->timestamps();

            $table->foreign('guru_id')
                ->references('id')
                ->on('gurus')
                ->onDelete('CASCADE');
            
            $table->foreign('siswa_id')
                ->references('id')
                ->on('siswas')
                ->onDelete('CASCADE');
            
            $table->foreign('jadwal_id')
                ->references('id')
                ->on('jadwals')
                ->onDelete('CASCADE');
            
            $table->foreign('hari_id')
                ->references('id')
                ->on('haris')
                ->onDelete('CASCADE');

            $table->foreign('kelas_id')
                ->references('id')
                ->on('kelas')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('absensis');
    }
}
