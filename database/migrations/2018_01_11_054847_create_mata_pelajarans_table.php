<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMataPelajaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mata_pelajarans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('jurusan_id')->unsigned();
            $table->string('kode_mat_pel');
            $table->string('jenis_mat_pel');
            $table->string('mata_pelajaran');
            $table->string('deskripsi');
            $table->string('tahun_ajaran');
            $table->timestamps();

            $table->foreign('jurusan_id')
                ->references('id')
                ->on('jurusans')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mata_pelajarans');
    }
}
