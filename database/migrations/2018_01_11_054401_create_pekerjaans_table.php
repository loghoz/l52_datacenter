<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePekerjaansTable extends Migration
{

    public function up()
    {
        Schema::create('pekerjaans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pekerjaan');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('pekerjaans');
    }
}
