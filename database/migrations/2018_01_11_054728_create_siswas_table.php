<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('agama_id')->unsigned();
            $table->integer('kelas_id')->unsigned();
            $table->string('no_induk');
            $table->string('nama');
            $table->string('tahun_ajaran');
            $table->string('hapus');
            $table->timestamps();

            $table->foreign('agama_id')
                ->references('id')
                ->on('agamas')
                ->onDelete('CASCADE');

            $table->foreign('kelas_id')
                ->references('id')
                ->on('kelas')
                ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('siswas');
    }
}
