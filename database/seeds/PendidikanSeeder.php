<?php

use Illuminate\Database\Seeder;
use App\Pendidikan;

class PendidikanSeeder extends Seeder
{
    public function run()
    {
        Pendidikan::create([
            'pendidikan' => '-'
        ]);
        Pendidikan::create([
            'pendidikan' => 'Tidak Sekolah'
        ]);
        Pendidikan::create([
            'pendidikan' => 'Putus SD'
        ]);
        Pendidikan::create([
            'pendidikan' => 'SD Sederajat'
        ]);
        Pendidikan::create([
            'pendidikan' => 'SMP Sederajat'
        ]);
        Pendidikan::create([
            'pendidikan' => 'SMA Sederajat'
        ]);
        Pendidikan::create([
            'pendidikan' => 'D1'
        ]);
        Pendidikan::create([
            'pendidikan' => 'D2'
        ]);
        Pendidikan::create([
            'pendidikan' => 'D3'
        ]);
        Pendidikan::create([
            'pendidikan' => 'S1'
        ]);
        Pendidikan::create([
            'pendidikan' => 'S2'
        ]);
        Pendidikan::create([
            'pendidikan' => 'S3'
        ]);
    }
}
