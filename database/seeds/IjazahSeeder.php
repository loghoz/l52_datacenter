<?php

use Illuminate\Database\Seeder;
use App\Ijazah;

class IjazahSeeder extends Seeder
{
    public function run()
    {
        Ijazah::create([
            'ijazah' => 'D1'
        ]);
        Ijazah::create([
            'ijazah' => 'D2'
        ]);
        Ijazah::create([
            'ijazah' => 'D3'
        ]);
        Ijazah::create([
            'ijazah' => 'D4'
        ]);
        Ijazah::create([
            'ijazah' => 'S1'
        ]);
        Ijazah::create([
            'ijazah' => 'S2'
        ]);
        Ijazah::create([
            'ijazah' => 'S3'
        ]);
    }
}
