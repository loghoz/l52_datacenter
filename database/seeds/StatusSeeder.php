<?php

use Illuminate\Database\Seeder;
use App\Status;

class StatusSeeder extends Seeder
{
    public function run()
    {
        Status::create([
            'status' => 'GTY'
        ]);
        Status::create([
            'status' => 'GTTY'
        ]);
        Status::create([
            'status' => 'PTY'
        ]);
        Status::create([
            'status' => 'PTTY'
        ]);
    }
}
