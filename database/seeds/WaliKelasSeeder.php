<?php

use Illuminate\Database\Seeder;
use App\WaliKelas;

class WaliKelasSeeder extends Seeder
{
    public function run()
    {
        //erwin
        WaliKelas::create([
            'guru_id'       => '2',
            'kelas_id'      => '31',
            'tahun_ajaran'  => '2017 / 2018',
        ]);

        //tria
        WaliKelas::create([
            'guru_id'       => '3',
            'kelas_id'      => '32',
            'tahun_ajaran'  => '2017 / 2018',
        ]);
    }
}
