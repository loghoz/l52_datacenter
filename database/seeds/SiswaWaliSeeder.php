<?php

use Illuminate\Database\Seeder;
use App\SiswaWali;

class SiswaWaliSeeder extends Seeder
{
    public function run()
    {
        //anggi
        SiswaWali::create([
            'no_induk'          => '1111',
            'ayah_nama'         => 'yeah',
            'ayah_tl'           => '1957',
            'ayah_pekerjaan'    => 'yeah',
            'ayah_pendidikan'   => 'yeah',
            'ayah_penghasilan'  => 'yeah',
            'ayah_telp'         => '089631073926',
            'ibu_nama'          => 'yeah',
            'ibu_tl'            => '1960',
            'ibu_pekerjaan'     => 'yeah',
            'ibu_pendidikan'    => 'yeah',
            'ibu_penghasilan'   => 'yeah',
            'ibu_telp'          => '089631073926',
            'wali_nama'         => 'yeah',
            'wali_tl'           => '1963',
            'wali_pekerjaan'    => 'yeah',
            'wali_pendidikan'   => 'yeah',
            'wali_penghasilan'  => 'yeah',
            'wali_telp'         => '089631073926'
        ]);

        //nurkarin
        SiswaWali::create([
            'no_induk'          => '1112',
            'ayah_nama'         => 'yeah',
            'ayah_tl'           => '1957',
            'ayah_pekerjaan'    => 'yeah',
            'ayah_pendidikan'   => 'yeah',
            'ayah_penghasilan'  => 'yeah',
            'ayah_telp'         => '089631073926',
            'ibu_nama'          => 'yeah',
            'ibu_tl'            => '1960',
            'ibu_pekerjaan'     => 'yeah',
            'ibu_pendidikan'    => 'yeah',
            'ibu_penghasilan'   => 'yeah',
            'ibu_telp'          => '089631073926',
            'wali_nama'         => 'yeah',
            'wali_tl'           => '1963',
            'wali_pekerjaan'    => 'yeah',
            'wali_pendidikan'   => 'yeah',
            'wali_penghasilan'  => 'yeah',
            'wali_telp'         => '089631073926'
        ]);

        for ($i=0; $i < 15 ; $i++) { 
            SiswaWali::create([
                'no_induk'          => 1113+$i,
                'ayah_nama'         => 'yeah',
                'ayah_tl'           => '1957',
                'ayah_pekerjaan'    => 'yeah',
                'ayah_pendidikan'   => 'yeah',
                'ayah_penghasilan'  => 'yeah',
                'ayah_telp'         => '089631073926',
                'ibu_nama'          => 'yeah',
                'ibu_tl'            => '1960',
                'ibu_pekerjaan'     => 'yeah',
                'ibu_pendidikan'    => 'yeah',
                'ibu_penghasilan'   => 'yeah',
                'ibu_telp'          => '089631073926',
                'wali_nama'         => 'yeah',
                'wali_tl'           => '1963',
                'wali_pekerjaan'    => 'yeah',
                'wali_pendidikan'   => 'yeah',
                'wali_penghasilan'  => 'yeah',
                'wali_telp'         => '089631073926'
            ]);
        }
    }
}
