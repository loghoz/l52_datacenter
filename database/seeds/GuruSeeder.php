<?php

use Illuminate\Database\Seeder;
use App\Guru;

class GuruSeeder extends Seeder
{
    public function run()
    {
        //default
        Guru::create([
            'agama_id'      => '1',
            'no_induk'      => '999999',
            'nama'          =>'-',
            'jabatan_id'    => '1',
            'ijazah_id'     => '1',
            'status_id'     => '1',
            'tahun_ajaran'  => '-',
            'hapus'         => '0',
        ]);

        //erwin
        Guru::create([
            'agama_id'      => '1',
            'no_induk'      => '111111',
            'nama'          =>'Erwin Dianto',
            'jabatan_id'    => '6',
            'ijazah_id'     => '1',
            'status_id'     => '1',
            'tahun_ajaran'  => '2017 / 2018',
            'hapus'         => '0',
        ]);

        //tria
        Guru::create([
            'agama_id'      => '1',
            'no_induk'      => '111112',
            'nama'          =>'Tria Dewi Ratnasari',
            'jabatan_id'    => '6',
            'ijazah_id'     => '1',
            'status_id'     => '1',
            'tahun_ajaran'  => '2017 / 2018',
            'hapus'         => '0',
        ]);

        for ($i=0; $i < 10 ; $i++) { 
            Guru::create([
                'agama_id'      => '1',
                'no_induk'      => 111113+$i,
                'nama'          => 'User Guru '.$i,
                'jabatan_id'    => '6',
                'ijazah_id'     => '1',
                'status_id'     => '2',
                'tahun_ajaran'  => '2017 / 2018',
                'hapus'         => '0',
            ]);
        }
        
    }
}
