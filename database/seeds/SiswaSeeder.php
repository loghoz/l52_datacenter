<?php

use Illuminate\Database\Seeder;
use App\Siswa;

class SiswaSeeder extends Seeder
{
    public function run()
    {
        //anggi bastiansyah
        Siswa::create([
            'no_induk'      => '1111',
            'nama'          => 'Anggi Bastiansyah',
            'kelas_id'      => '31',
            'agama_id'      => '1',
            'tahun_ajaran'  => '2017 / 2018',
            'hapus'         => '0',
        ]);

        //nurkarin
        Siswa::create([
            'no_induk'      => '1112',
            'nama'          => 'Nurkarin',
            'kelas_id'      => '32',
            'agama_id'      => '1',
            'tahun_ajaran'  => '2017 / 2018',
            'hapus'         => '0',
        ]);

        for ($i=0; $i < 15; $i++) { 
            //user loop
            Siswa::create([
                'no_induk'      => 1113+$i,
                'nama'          => 'User Siswa '.$i,
                'kelas_id'      => '31',
                'agama_id'      => '1',
                'tahun_ajaran'  => '2017 / 2018',
                'hapus'         => '0',
            ]);
        }
    }
}
