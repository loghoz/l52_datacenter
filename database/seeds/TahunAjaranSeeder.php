<?php

use Illuminate\Database\Seeder;
use App\TahunAjaran;

class TahunAjaranSeeder extends Seeder
{
    public function run()
    {
        TahunAjaran::create(['tahun_ajaran' => '2017 / 2018']);
        TahunAjaran::create(['tahun_ajaran' => '2018 / 2019']);
        TahunAjaran::create(['tahun_ajaran' => '2019 / 2020']);
        TahunAjaran::create(['tahun_ajaran' => '2020 / 2021']);
        TahunAjaran::create(['tahun_ajaran' => '2021 / 2022']);
        TahunAjaran::create(['tahun_ajaran' => '2022 / 2023']);
        TahunAjaran::create(['tahun_ajaran' => '2023 / 2024']);
        TahunAjaran::create(['tahun_ajaran' => '2024 / 2025']);
    }
}
