<?php

use Illuminate\Database\Seeder;
use App\GuruPiket;

class GuruPiketSeeder extends Seeder
{

    public function run()
    {
        //erwin
        GuruPiket::create([
            'guru_id'       => '2',
            'hari_id'       => '4',
            'tahun_ajaran'  => '2017 / 2018',
        ]);

        //tria
        GuruPiket::create([
            'guru_id'       => '3',
            'hari_id'       => '4',
            'tahun_ajaran'  => '2017 / 2018',
        ]);
    }
}
