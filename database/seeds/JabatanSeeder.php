<?php

use Illuminate\Database\Seeder;
use App\Jabatan;

class JabatanSeeder extends Seeder
{
    public function run()
    {

        Jabatan::create([
            'jabatan' => 'Admin'
        ]);
        Jabatan::create([
            'jabatan' => 'Kepala Sekolah'
        ]);
        Jabatan::create([
            'jabatan' => 'Wakil Kepala Sekolah'
        ]);
        Jabatan::create([
            'jabatan' => 'Kepala Jurusan'
        ]);
        Jabatan::create([
            'jabatan' => 'Bimbingan Konseling'
        ]);
        Jabatan::create([
            'jabatan' => 'Guru'
        ]);
        Jabatan::create([
            'jabatan' => 'Staf Perpustakaan'
        ]);
        Jabatan::create([
            'jabatan' => 'Staf Tata Usaha'
        ]);
        Jabatan::create([
            'jabatan' => 'Op. Inventaris'
        ]);
    }
}
