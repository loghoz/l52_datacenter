<?php

use Illuminate\Database\Seeder;
use App\GuruMeta;

class GuruMetaSeeder extends Seeder
{
    public function run()
    {
        //default
        GuruMeta::create([
            'no_induk' => '999999',
            'nuptk' => '123456789',
            'jk'=>'laki-laki',
            'tempat_lahir'=>'-',
            'tanggal_lahir'=>'1992-01-08',
            'ijazah_jurusan'=>'Teknik Komputer',
            'tahun_tugas'=>'2013',
            'alamat'=>'sdsad',
            'rt'=>'1',
            'rw'=>'1',
            'desa'=>'sdsad',
            'kecamatan'=>'sdsad',
            'kabupaten'=>'sdsad',
            'provinsi'=>'sdsad',
            'kode_pos'=>'35372',
            'telp'=>'+6289631073926',
            'photo'=>'avatar5.png',
        ]);

        //erwin
        GuruMeta::create([
            'no_induk' => '111111',
            'nuptk' => '123456789',
            'jk'=>'laki-laki',
            'tempat_lahir'=>'Krandegan',
            'tanggal_lahir'=>'1992-01-08',
            'ijazah_jurusan'=>'Teknik Komputer',
            'tahun_tugas'=>'2013',
            'alamat'=>'Jl. Raya Ahmad Yani Gadingrejo Timur',
            'rt'=>'1',
            'rw'=>'1',
            'desa'=>'Pekon Gadingrejo Timur',
            'kecamatan'=>'Gadingrejo',
            'kabupaten'=>'Pringsewu',
            'provinsi'=>'Lampung',
            'kode_pos'=>'35372',
            'telp'=>'+6289631073926',
            'photo'=>'avatar5.png',
        ]);

        //tria
        GuruMeta::create([
            'no_induk' => '111112',
            'nuptk' => '123456789',
            'jk'=>'perempuan',
            'tempat_lahir'=>'Penengahan',
            'tanggal_lahir'=>'1992-01-08',
            'ijazah_jurusan'=>'Teknik Informatika',
            'tahun_tugas'=>'2015',
            'alamat'=>'Jl. Raya Penengahan Gedongtataan',
            'rt'=>'1',
            'rw'=>'1',
            'desa'=>'Penengahan',
            'kecamatan'=>'Gedongtataan',
            'kabupaten'=>'Pesawaran',
            'provinsi'=>'Lampung',
            'kode_pos'=>'35371',
            'telp'=>'+6289631073232',
            'photo'=>'avatar2.png',
        ]);

        for ($i=0; $i < 10 ; $i++) { 
            GuruMeta::create([
                'no_induk'      => 111113+$i,
                'nuptk'         => '123456789',
                'jk'            => 'laki-laki',
                'tempat_lahir'  => 'Penengahan',
                'tanggal_lahir' => '1992-01-08',
                'ijazah_jurusan'=> 'Teknik Informatika',
                'tahun_tugas'   => '2015',
                'alamat'        => 'Jl. Raya Penengahan Gedongtataan',
                'rt'            => '1',
                'rw'            => '1',
                'desa'          => 'Penengahan',
                'kecamatan'     => 'Gedongtataan',
                'kabupaten'     => 'Pesawaran',
                'provinsi'      => 'Lampung',
                'kode_pos'      => '35371',
                'telp'          => '+6289631073232',
                'photo'         => 'avatar5.png',
            ]);
        }
    }
}
