<?php

use Illuminate\Database\Seeder;
use App\Agama;

class AgamaSeeder extends Seeder
{

    public function run()
    {
        Agama::create([
            'agama'=>'Islam'
        ]);
        Agama::create([
            'agama'=>'Kristen'
        ]);
        Agama::create([
            'agama'=>'Katholik'
        ]);
        Agama::create([
            'agama'=>'Hindu'
        ]);
        Agama::create([
            'agama'=>'Budha'
        ]);
        Agama::create([
            'agama'=>'Lainnya'
        ]);
    }
}
