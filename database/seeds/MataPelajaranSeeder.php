<?php

use Illuminate\Database\Seeder;
use App\MataPelajaran;

class MataPelajaranSeeder extends Seeder
{
    public function run()
    {
        for ($i=0; $i < 9 ; $i++) { 
            $j = $i+1;
            MataPelajaran::create([
                'jurusan_id'    => '5',
                'kode_mat_pel'  => 'TK0'.$j,
                'jenis_mat_pel' => 'Produktif',
                'mata_pelajaran'=> 'Teknik'.$j,
                'deskripsi'     => 'Mata Pelajaran Teknik Komputer Jaringan',
                'tahun_ajaran'  => '2017 / 2018'
            ]);
        }

        MataPelajaran::create([
            'jurusan_id'    => '1',
            'kode_mat_pel'  => '02NA',
            'jenis_mat_pel' => 'Normatif dan Adaptif',
            'mata_pelajaran'=> 'PPKN',
            'deskripsi'     =>'Tata Krama',
            'tahun_ajaran'  =>'2017 / 2018'
        ]);
    }
}
