<?php

use Illuminate\Database\Seeder;
use App\Pekerjaan;

class PekerjaanSeeder extends Seeder
{
    public function run()
    {
        Pekerjaan::create([
            'pekerjaan' => '-'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Tidak Bekerja'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Nelayan'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Petani'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Peternak'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'PNS/TNI/Polri'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Karyawan Swasta'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Pedagang Kecil'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Pedagang Besar'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Wiraswasta'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Wirausaha'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Buruh'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Pensiunan'
        ]);
        Pekerjaan::create([
            'pekerjaan' => 'Lainnya'
        ]);
    }
}
