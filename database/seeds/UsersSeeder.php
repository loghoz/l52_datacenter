<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{

    public function run()
    {
        //sample admin
        App\User::create([
            'no_induk' => '9999999999',
            'email' => 'admin@smkpelitapesawaran.sch.id',
            'username' => 'admin',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('admin@smkpelitapesawaran.sch.id'),
            'role' => 'admin'
        ]);

        // sample guru
        //erwin
        App\User::create([
            'no_induk' => '111111',
            'email' => 'erwin@smkpelitapesawaran.net',
            'username' => 'erwin',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('erwin@smkpelitapesawaran.sch.id'),
            'role' => 'guru'
        ]);

        //tria
        App\User::create([
            'no_induk' => '111112',
            'email' => 'tria@smkpelitapesawaran.net',
            'username' => 'tria',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('tria@smkpelitapesawaran.sch.id'),
            'role' => 'guru'
        ]);

        // sample siswa
        App\User::create([
            'no_induk' => '1111',
            'email' => '1111@smkpelitapesawaran.net',
            'username' => '1111',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('1111@smkpelitapesawaran.sch.id'),
            'role' => 'siswa'
        ]);

        App\User::create([
            'no_induk' => '1112',
            'email' => '1112@smkpelitapesawaran.net',
            'username' => '1112',
            'password' => bcrypt('1sampai9'),
            'api_token' => bcrypt('1112@smkpelitapesawaran.sch.id'),
            'role' => 'siswa'
        ]);

        for ($i=0; $i < 10 ; $i++) { 
            App\User::create([
                'no_induk'  => 111113+$i,
                'email'     => 'userguru'.$i.'@smkpelitapesawaran.net',
                'username'  => 'userguru'.$i,
                'password'  => bcrypt('1sampai9'),
                'api_token' => bcrypt('user'.$i.'@smkpelitapesawaran.sch.id'),
                'role'      => 'guru'
            ]);
        }

        for ($i=0; $i < 15; $i++) { 
            App\User::create([
                'no_induk'  => 1113+$i,
                'email'     => 'usersiswa'.$i.'@smkpelitapesawaran.net',
                'username'  => 'usersiswa'.$i,
                'password'  => bcrypt('1sampai9'),
                'api_token' => bcrypt('usersiswa'.$i.'@smkpelitapesawaran.sch.id'),
                'role'      => 'siswa'
            ]);
        }
    }
}
