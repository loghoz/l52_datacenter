<?php

use Illuminate\Database\Seeder;
use App\SiswaMeta;

class SiswaMetaSeeder extends Seeder
{
    public function run()
    {
        //anggi
        SiswaMeta::create([
            'no_induk'      => '1111',
            'nisn'          => '111111',
            'jk'            => 'laki-laki',
            'tempat_lahir'  => 'Sukamarga',
            'tanggal_lahir' => '1992-01-08',
            'alamat'        => 'Jl. Raya Penengahan Sukamarga',
            'rt'            => '1',
            'rw'            => '1',
            'desa'          => 'Sukamarga',
            'kecamatan'     => 'Gedongtataan',
            'kabupaten'     => 'Pesawaran',
            'provinsi'      => 'Lampung',
            'kode_pos'      => '35372',
            'telp'          =>'+6289631073926',
            'photo'         =>'avatar1.png',
        ]);

        //nurkarin
        SiswaMeta::create([
            'no_induk'      => '1112',
            'nisn'          => '111111',
            'jk'            => 'perempuan',
            'tempat_lahir'  => 'Sukamarga',
            'tanggal_lahir' => '1992-01-08',
            'alamat'        => 'Jl. Raya Penengahan Sukamarga',
            'rt'            => '1',
            'rw'            => '1',
            'desa'          => 'Sukamarga',
            'kecamatan'     => 'Gedongtataan',
            'kabupaten'     => 'Pesawaran',
            'provinsi'      => 'Lampung',
            'kode_pos'      => '35372',
            'telp'          =>'+6289631073926',
            'photo'         =>'avatar3.png',
        ]);

        for ($i=0; $i < 15 ; $i++) { 
            SiswaMeta::create([
                'no_induk'      => 1113+$i,
                'nisn'          => '111111',
                'jk'            => 'laki-laki',
                'tempat_lahir'  => 'Sukamarga',
                'tanggal_lahir' => '1992-01-08',
                'alamat'        => 'Jl. Raya Penengahan Sukamarga',
                'rt'            => '1',
                'rw'            => '1',
                'desa'          => 'Sukamarga',
                'kecamatan'     => 'Gedongtataan',
                'kabupaten'     => 'Pesawaran',
                'provinsi'      => 'Lampung',
                'kode_pos'      => '35372',
                'telp'          => '+6289631073926',
                'photo'         => 'avatar1.png',
            ]);
        }
    }
}
